# clubmaster-docs

prepare on ubuntu

sudo apt install build-essential
sudo apt install python3-pip
sudo apt install python3-imagesize python3-snowballstemmer python3-jinja2 python3-babel python3-alabaster

#sudo pip install sphinx --break-system-packages
sudo pip install sphinx-rtd-theme --break-system-packages

mkdir _static/

make html

google-chrome ./_build/html/index.html

Verifone
========

Her er en gennemgående guide til opsætning af din nye Verifone P400 og M400 med OptikPartner.


Fejlsøgning af terminal
-----------------------

Hvis der er problemer med en terminal som allerede kører.

- Prøv først, tænd og sluk terminalen
- Check forbindelsen er oppe (`Kontroller WIFI er oppe`_)
- Derefter kontroller ECR kommunikationen er sat korrekt op, den bliver engang i mellem reset hos Verifone.


Ved konstant afvisning
~~~~~~~~~~~~~~~~~~~~~~

Prøv evt. at manuelt afstemme terminalen.

Fejler det, prøv sikre at der ikke ligger transaktioner og hænger.

Dette kan kontrolleres i hovedmenuen, findes der transaktioner skal man:

1. Gå et password af verifone til at slette dem
2. Slette alle hængende transaktioner
3. Ringe til verifone og lade dem synkronisere terminalen


Kontroller først version
------------------------

Sikre at din terminal understøtter ECP forbindelse.

    Sæt strøm til terminalen og vent til startskærmen (se billede under)


ECR kompatibel
^^^^^^^^^^^^^^

Først skal du finde ud af om din version understøtter "ECR", eller om den kører "Standalone".


    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"

    **Har du her menupunktet 5 som hedder "ECR" er du klar, ellers skal din terminal opgraderes.**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


GPRS eller WIFI
^^^^^^^^^^^^^^^

Når du er gennem øverste step, kan du fortsætte og finde ud af hvilken guide du skal bruge.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 1, "Communications"

    **Har din model her GPRS, så gå til guiden om GPRS, ellers brug WIFI.**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Opsætning af terminal med GPRS
------------------------------

Hvis du har GPRS skal du.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 2, "Network fallback"
    - Tast 1, "Default"

    **Sikre at der er flueben i GPRS** (ved wifi guide, vælg wifi)

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Sæt nu ECR adresse

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 5, "ECR"
    - Tast 1, "Set ECR Address"

    **Få dette nummer fra OptikPartner**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Opsætning af ECR port

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 5, "ECR"
    - Tast 2, "Set ECR Port"

    **Få dette nummer fra OptikPartner**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Kontroller communication device

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 5, "ECR"
    - Tast 3, "Choose comm. device"

    **Sikre at denne står til GPRS** (ved wifi guide, vælg wifi)


Når du er færdig skal der gerne stå blot ordet **VERIFONE** midt på terminalen.

Er noget gået galt, så vil der stadig stå **Waiting for ECR connection**, start derfor forfra.

Fortsæt nu til konfiguration i OptikPartner nederst i denne guide.


Opsætning af terminal med Wifi
------------------------------

Hvis du har en med WIFI, skal du først koble den til dit netværk, derefter følger du blot guiden til GPRS blot ved at vælge WIFI i stedet for GPRS ved forbindelsestype.


Opsætning af WIFI
^^^^^^^^^^^^^^^^^

Hav din adgangskode til netværket parat.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 1, "Communication"
    - Find WIFI og klik på den
    - Klik på "Configuration"
    - Klik på "WiFi scan"
    - Find dit netværk og klik på det
    - Klik nu på "Add"
    - Klik på "Pre-Shared-Key (PSK)
    - Skriv din adgangskode til netværket her
    - Klik derefter "OK"
    - Klik nu "Save"
    - Klik nu "OK"
    - Klik nu til spørgsmålet "Apply settings on interface", "Yes"

    Nu arbejder terminalen på at komme på netværket, kontroller at der står **Interface started** med grøn skrift, klik derefter "OK"

    Hvis ikke dette er tilfældet, genstart terminalen og kontroller om WIFI kommer op automatisk, ifølge guiden herunder. Ellers prøv forfra.

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Kontroller WIFI er oppe
^^^^^^^^^^^^^^^^^^^^^^^

Sikre at terminalen er logget på dit netværk.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 1, "Communication"

    **Sikre at der ud for WIFI nu står "Up"**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.


Hvis WIFI ikke er oppe, kan du prøve at aktivere WIFI manuelt

    Gå i administration på terminalen

    - Tast 4+6
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast 1, "Communication"
    - Klik på teksten "WIFI"
    - Vælg "Start Network Interface"
    - Der vil nu stå "In Process", evt. får du en liste over netværk, klik på det ønsket.

    **Vent på besked om terminalen er logget på eller ej, hvis fortsat fejl, kontroller netværksopsætningen og prøv igen**

    Gå nu tilbage til start på **RØD KNAP** gentagende gange på terminalen.

**Du er nu færdig med denne del, gå til guiden for GPRS, men husk at erstatte GPRS med WIFI når det er beskrevet.**


Manual afstemning
-----------------

Sikre at terminalen er logget på dit netværk.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 1, "Settlements"
    - Tast 1, "Reconciliation"

    **Efter kort til vil du se en grøn SUCCESS besked**


Billeder
--------

Billede af terminalen som **IKKE ER KLAR**

.. image:: /images/verifone-not-ready.jpg
   :width: 400


Billede af terminalen som **ER KLAR**

.. image:: /images/verifone-ready.jpg
   :width: 400


Opsætning i OptikPartner
------------------------

For at få terminalen til at fungere i OptikPartner, følg disse steps.

- Gå til Indstillinger
- Gå til Betalingsmetoder.
- Opret en ny betalingsmetode


Du skal bruge følgende indstillinger for at terminalen vil fungere.

:Type: Verifone P400
:Serial: (serienummer på terminalen)
:Ip addresse: **Få dette udleveret af OptikPartner**


Du vil nu have en ny betalingsknap på ekspeditionen.


Opsætning af filtre
^^^^^^^^^^^^^^^^^^^

Hvis du ønsker at dankort, visa eller andre kort kommer ind på forskellige konti i OptikPartner, kan du benytte dig af filtre i **Indstillinger > Betalingsmetoder**.

Som udgangspunkt kommer alle indbetalinger over i økonomisystemet via feltet **Kontoplan**.


Ønsker at splitte kort op på forskellige konti i dit regnskabsystem, så har du mulighed for at lave filtre, som overskriver standard opsætningen.


Via Verifone kan vi separer via følgende kort:

- DanKort
- MasterCard
- MasterCard DK
- Visa


Så hvis du ønsker at sætte Dankort indbetalinger på en anden konto, kan du oprette et filter hvor du udfylder:

:Søgning: DanKort
:Kontoplan: **Vælg en valgfri konto fra din kontoplan**


Og alle indbetalinger som ikke rammer et filter, vil altså ryge ind på standard kontoplanen.


.. image:: /images/verifone-filter.png
   :width: 800


Lyd styring
-----------

Hvis du ønsker at få mere eller mindre lyd på din Verifone terminal.

    Gå i administration på terminalen

    - Tast 4+6 samtidig og skriv koden "1234"
    - Tast 2, "Administration"
    - Tast 1, "Settings"
    - Tast ned til side 2
    - Tast 2, Sound volume

Her kan du indstille hvor mange procent lyden skal være på.


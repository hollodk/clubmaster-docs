USB sleep mode
==============

Sådan deaktivere du USB sleep mode, som ofte kan forårsage udstyr stopper med at fungere efter ikke være brugt i noget tid.


Windows 10
==========

Deaktiver USB sleep mode
^^^^^^^^^^^^^^^^^^^^^^^^

Først åben kontrol panelet

.. image:: /images/hardware-control-panel.png
   :width: 800


Gå til hardware and Sound

.. image:: /images/hardware-hardware-sound.png
   :width: 800


Under punktet **Power Options** vælg her **Edit power plan**

.. image:: /images/hardware-power-plan.png
   :width: 800


Gå til **Change advanced power settings**

Find USB settings i listen, og vælg **On battery** og **Plugged in** skal begge være disabled.


.. image:: /images/hardware-power-plan-disabled.png
   :width: 800



Zebra GX430t
============

Her er guides til hvordan du kommer igang med en Zebra GX430t.


Installation
------------

Her har vi lavet en udførlig slavisk guide som du altid kan følge for at få din Zebra printer til at fungere.



1. Sæt først strøm til din printer.


2. Kobl derefter usb kabel fra din printer til computeren.


3. Når du er færdig skal du downloade zebras seneste driver:
   https://www.zebra.com/us/en/support-downloads/printers/desktop/gx430t.html

   .. image:: /images/zebra/01-download.png
      :width: 800

4. Godkend deres licens, og start programmet som du netop har downloadet.

   .. image:: /images/zebra/02-download.png
      :width: 800


5. Det første valg du skal træffe er hvilken port på computeren din printer sidder til, det vil typisk være **USB**.

   .. image:: /images/zebra/03-open.png
      :width: 800

6. Oftest finder installationssoftwaren ikke din printer, vælg derfor **Manual Install**.

   .. image:: /images/zebra/04-manual-install.png
      :width: 800

7. Vælg her **Install Printer** for at installere din nye printer.

   .. image:: /images/zebra/05-install-choose.png
      :width: 800

8. Når du skal vælge driver, så bladre du ned i listen indtil du finder **ZDesigner GX430t** og klikker næste.

   .. image:: /images/zebra/06-driver.png
      :width: 800

9. Så vælger du på næste side **USB001** som port, og klikker så næste.

   .. image:: /images/zebra/07-port.png
      :width: 800

10. Når du er færdig, så klikker du blot på **Finish** knappen, og din printer vil nu være installeret.

   .. image:: /images/zebra/08-install-finish.png
      :width: 800


Opsætning af printer
--------------------

Din printer er nu installeret, men for at sikre at din printer fungere optimalt, så skal vi gerne ind og sætte driveren korrekt op, følg med her.


1. For at komme ind i Zebras kontrolpanel så finder du nu på dit skrivebord et nyt ikon som hedder **Zebra Setup Utilities**.
   Klik på dette, og du vil se følgende menu.

2. Dette punkt er ikke nødvendigt, det er blot for at sikre du kan kommunikere med din printer, så spring blot dette over hvis du har modtaget en ny printer.
   Sikre at din installeret printer er markeret som her, og vælg så **Open Communication With Printer**.

   .. image:: /images/zebra/09-communication-open.png
      :width: 800

3. Her åbner sig nu en boks, hvor du kan sende kommandoer til din printer, det er vigtigt i forhold til at teste kommunikationen som her.
   Skriv: ^XA^HH^XZ
   Klik derefter på **Send To Printer**, og hvis der kommer noget tekst i nederst felt, så fungere kommunikationen med printeren, og du har gennemført ovenstående opgaver korrekt.

   Koder der er nyttige:
   ^XA^HH^XZ brug denne for at få status informationer fra printeren, vil Zebra support ofte spørge efter ved support.
   ^XA^JUF^XZ udfør en factory reset
   ^XA^JUN^XZ bruges til at nulstille printerens netværks opsætning hvis dette bruges.

   .. image:: /images/zebra/10-communication-verification.png
      :width: 800

4. Hvis du har testet som ovenstående kommer du tilbage til Zebra menuen, dobbelt klik nu på din printer som vist på illustrationen.

   .. image:: /images/zebra/11-settings.png
      :width: 800

5. Herefter åbner sig dine indstillinger, her er et par ting du skal være opmærksom står korrekt.

   Ofte er teksten lidt uklar, du kan med fordel skrue **Darkness** så højt op som muligt, i vores tilfælde er dette 30, kan variere blandt printere.
   Hvis tiden ikke er vigtig for dig, kan du sagtens stille **Speed** ned, det giver ofte mere præcis print.

   .. image:: /images/zebra/12-options.png
      :width: 800

6. Under fanen **Advanced Setup** indstiller du alt efter hvilken label type du bruger.

   I de fleste tilfælde med Zebra skal du indstille denne på følgende:
   Media Type skal ved farvebånd være **Thermal Transfer**, og i tilfælde af termisk print være **Thermal Direct**.
   Tracking Mode skal stå til **Web Sensing**, det kan leverandøren af dine labels altid svare på

   .. image:: /images/zebra/13-advanced-setup.png
      :width: 800


Printer test
------------

Nu er din printer klar til at blive brugt :)

Alle printere og labels er forskellige, det kan derfor være lidt besværligt at finde den korrekte indstilling for margins og størrelse.

Vi har lavet noget software hvor du let kan teste dine indstillinger af, gå derfor til https://print.clubmaster.org/index.php/default.

Vælg først knappen **Fetch printer**, så kommer der en liste af printere til højre på skærmen.

   .. image:: /images/zebra/14-print-fetch.png
      :width: 800


For at teste korrekt opsætning skal du gøre følgende:

Vælg først den printer i højre side du vil benytte dig af, det vil typisk være en med navnet **Zebra**.

For at teste klikker du blot på **Print sample**, og din printer vil nu skrive en label ud.

Vi har noget simpelt markeringstekst, sådan du kan se hvor printeren printer henne.

Du kan på din label se følgende tekst.

    | 1 o p t i k p a r t n e r 2 o p t i k p a r t n e r

Målet er at der i øverst højre hjørne bør stå **1 o p t i k**, så er printeren indstillet korrekt.

Du kan nu korrigere højde, bredde, skriftstørrelse samt margins indtil den passer præcis på din kabel.

   .. image:: /images/zebra/15-print-sample.png
      :width: 800


Afhjælpning af problemer
------------------------

Der kan være mange tilfælde hvorfor printeren giver problemer, der er mange mekaniske dele i printeren, så engang i mellem vil de kræve en form for vedligeholdelse når det drejer sig om farvebånd.

Vi har lagt løsningerne i kronologisk rækkefølgende for hvordan du skal prøve at få printeren tilbage på rette køl:

- Genstart
- Rengøring
- Kalibrering
- Factory reset
- Afinstaller og reinstaller din printer


Genstart din computer
^^^^^^^^^^^^^^^^^^^^^

Start med at genstarte din computer, det fjerner oftest sporadisk opstået fejl som let kan opstå med printere.

Prøv så igen at se om printeren fungere, gør den ikke det kan du fortsætte til næste punkter for at afhjælpe problemet.


Genstart af printer
^^^^^^^^^^^^^^^^^^^

Ved tilfælde af problemer med din Zebra printer, tag USB stikket ud af printeren og sluk for din printer.

Vent 10 sekunder og sæt så USB stikket tilbage i computeren mens printeren er slukket.

Når USB stikket er koblet ind i computeren igen tænd så for printeren.

Prøv så igen at se om printeren fungere, gør den ikke det kan du fortsætte til næste punkter for at afhjælpe problemet.


Rengøring
^^^^^^^^^

Printere sviner, de skal renses engang i mellem optimalt siger Zebra selv at ved hver label skifte bør man rense sin printer for at forlænge deres levetid.

Her har vi lagt en guide til hvordan du renser printerhovedet på både en Termisk printer og en farvebånds printer.

https://supportcommunity.zebra.com/s/article/G-Series--Cleaning-the-Printhead-With-Video?language=en_US


Kalibrering
^^^^^^^^^^^

I tilfælde af printeren ikke fungere optimalt, kan du starte med en rekalibrering af printeren.

1. Hold power knappen nede indstil den bliver 2 gange og slip derefter knappen.

2. Tillad printeren at køre et øjeblik mens den kalibrer.

3. Tryk på power knappen for at sende en label ud, du kan gentage dette step til den fungere optimalt.


Factory reset
^^^^^^^^^^^^^

Hvis det ikke afhjælper problemet, lav da en factory reset.

1. Hold power knappen nede til at den bliver 4 gange.

2. Tillad printeren at køre et øjeblik.

3. Du vil oftest være nødt til at gennemgå vores guide igen, og sikre at indstillingerne er korrekte, da disse vil blive nulstillet.


Afinstallation af zebra
-----------------------

I tilfælde af at intet fungere, prøv derefter helt forfra, afinstaller alt software som er relateret til Zebra, og start så forfra med vores guide.

Dette step kan være nødvendigt i tilfælde af der er kommet nye printer drivere som løser nylig opstået problemer med din computer.


1. Åben **Zebra Setup Utilities**, og vælg uninstall ud for den printer du ønsker at afinstallere.

2. Gå derefter til din startmenu og fint programmet **Add and remove programs**, og slet Zebra softwaren som ligger helt nede i bunden af listen.

3. Start derefter guiden forfra med at downloade Zebra software forfra og følge vores installations guide.


Opticon
=======

Sådan tilslutter du din Opticon til OptikPartner

#. Installer Opticon
#. Opret Barcode i OptikPartner
#. Test i OptikPartner


Installer Opticon
^^^^^^^^^^^^^^^^^

For at installere jeres Opticon skal I gå til Indstillinger, i indstillingerne vælger I enheder, når I kommer derind vælger I Tilføj bluetooth-enhed eller en anden enhed, så kommer der en fane op, hvor I vælger Bluetooth, og computeren søger nu efter ledige enheder.

I skal nu holde den lille knap nede på jeres Opticon i fem sekunder til den siger en lyd samt skifter farve.

Så skulle jeres Opticon meget gerne komme frem på den fane, som står og søger efter enheder, Når den kommer frem, trykker I blot på den, og den er nu sat op på computeren.


Find stregkoder i OptikPartner
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
For at finde et produkts stregkode i OptikPartner, skal man gå ind på det specifikke produkt. Hvorefter man vælger Varianter, her kan man printe en label med produktets stregkode.

En anden måde man kan få stregkoder på i OptikPartner er under Indstillinger - Produktstyring - Produktark efter det kommer man ind på en side, hvor man skal trykke Opret ny. Efter det kommer man ind på selve produktoversigten, her udfylder man navn på oversigten, og trykker i bunden af siden Tilføj standard.

Her kommer der en oversigt op med produkter, det man så skal udfylde er SKU numre, man skal her indtaste de SKU numre, man vil have stregkoder på. Når det er gjort trykker man Save.

Herefter kommer man ud på en oversigt, hvor man kan se et overblik over ens Produktark. Så trykker man Print på det Produktark, man lige har udfyldt, også får man en udprintet version af stregkoderne.


Test din Opticon i OptikPartner
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Når du har stregkoder og din Opticon er installeret, er du klar til at bruge den i ekspeditionen.

Det første, du skal gøre, er at gå til Ekspeditionen i OptikPartner. Herefter scanner du den stregkode, som skal ekspederes, og sådan kan du bruge din Opticon til at scanne varer ind i kassen.


Omkonfigurer scanner
^^^^^^^^^^^^^^^^^^^^

Brug følgende scan til at ændre indstillingerne for din scanner.

.. image:: /images/opticon-scan.jpg
   :width: 800


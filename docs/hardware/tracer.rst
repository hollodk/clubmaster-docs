Tracer
======

Små tips og tricks til brug af tracer.


Tips
----

Hvis du har problemer med traceren, er her nogen ting, du bør kontrollere først.


* Tjek at traceren sidder i samme USB-port, som den plejer.
* Tjek at du har åbnet OptikPartner tracerprogrammet.
* Tjek at du ikke har andre tracerprogrammer åben som f.eks. Hoya log eller andre tracerprogrammer.
* genstart computeren og tracer


Opsætning
---------

Opsætning med HOYA
------------------

For at komme igang skal du først have installeret HOYA's tracer software på din computer.

1. Sikre først at din tracer er sat op til OMA format, det kan du kontrollere med HOYA, eller hos din hardware leverandør.

2. Download HOYA's Tracer Manger ved at gå til https://dk.hoyailog.com/, gå til **Arbejdsstation > Downloads**, og vælg **Download TracerManager**.

3. Når du er færdig, gennemgå med HOYA at traceren fungerer i ilog.

4. Derefter gå til OptikPartner og vælg **Indstillinger > Optik > Tracer**, og lav en ny tracer.

5. Kopier nu de indstillinger som blev brugt til at sætte ilog op fra punkt 3, port nummer er 64342.

6. Derefter kan du tracer i OptikPartner via glasbestillingen.


Opsætning med ZEISS
-------------------

For at komme igang skal du først have installeret ZEISS's tracer software på din computer.

1. Sikre først at din tracer er sat op til OMA format, det kan du kontrollere med ZEISS, eller hos din hardware leverandør.

2. Download ZEISS's Trace software ved at gå til https://visustore.zeiss.com/, gå til **Indstillinger > Tracer konfiguration**, og vælg **Installer Tracer Client**.

3. Når du er færdig, gennemgå med ZEISS at traceren fungere i visustore.

4. Derefter gå til OptikPartner og vælg **Indstillinger > Optik > Tracer**, og lav en ny tracer.

5. Kopier nu de indstillinger som blev brugt til at sætte ilog op fra punkt 3, port nummer er 65500.

6. Derefter kan du tracer i OptikPartner via glasbestillingen.




Booking plugin
==============

Her er en indholdsfortegnelse af booking plugins.

.. toctree::
    :maxdepth: 1

    /docs/plugin/overview.rst
    /docs/plugin/facility_product.rst

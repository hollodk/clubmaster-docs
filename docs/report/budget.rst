Budget
======

Budgetter i OptikPartner
------------------------

Du har mulighed for at holde styr på dine budgetter i OptikPartner, du kan yderligere inddele dine rapporter i flere undergrupper hvis du har behov for finindeling.

Budgetterne tager udgangspunkt i din opsætning af produkt grupper, du kan nemt få et overblik over hvor meget du sælger i de forskellige produkt grupper.

Vær dog ekstra opmærksom på, hvis du har regnskabssynkronisering, at dine produktgrupper fungere med dit regnskabssystem.

For at finde budgetter i OptikPartner gå da til **Rapport > Budget**.

.. image:: /images/report-budget-overview.png
   :width: 800


Opret budget grupper
~~~~~~~~~~~~~~~~~~~~

Hvis du ønsker at kategorisere dine produktgrupper yderligere i eksempelvis Høre-/synsdel, så kan du bruge **Budget grupper**.

.. image:: /images/report-budget-group.png
   :width: 800


Opsætning af målsætning
~~~~~~~~~~~~~~~~~~~~~~~

Som standard viser vi **alle** produktgrupper, men de står alle til 0 i målsætning indtil du begynder at sætte tingene op.

Gå til **Rapport > Budget > Budget målsætning**.

Opret først en produktgruppe øverst i højre side på **Opret ny**, her vælger du en produkt gruppe du vil have med i budgetterne, samt en evt. budget gruppe hvis du bruger dette.

Herefter bliver du præsenteret for en stor tabel, hvor du kan indtaste målsætning for hver produktgrupper for hver måned pr år.

.. image:: /images/report-budget-goal.png
   :width: 800


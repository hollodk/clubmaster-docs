ResursBank
==========

Oprette ansøgning
^^^^^^^^^^^^^^^^^

Der er 2 måder at lave en ansøgning på ResursBank igennem OptikPartner.


Opret efter et salg
^^^^^^^^^^^^^^^^^^^

Når en kunde har købt en vare du ønsker at betale med ResursBank, så gennemfører du salget **På Konto**.

Derefter kan du når salget er gennemført se knappen **Vis salgsdetaljer**, eller gå til den via salgshistorikken.

Klik herefter på knappen **Finansieringsansøgning**.

.. image:: /images/resursbank-sale.png
   :width: 800


Herefter ser du et overblik med kundens lånemuligheder.

Lånestørrelsen kan du se øverst i vinduet, den tager som standard størrelsen på salget.

Derefter kommer der forskellige lånemuligheder, vælg den mulighed som passer din kunde bedst og klik **Ansøg Nu**.

.. image:: /images/resursbank-overview.png
   :width: 800


På næste side skal du indtaste stamdata på kunden, som udgangspunkt hiver vi informationerne fra kunden som har gennemført salget. Mangler der her nogen data, så indtaster du dem blot sammen med kunden.

Bekræft beløbet i højre side.

Når du er færdig klikker du blot **Send ansøgning**, og din ansøgning bliver automatisk sendt til ResursBank.

.. image:: /images/resursbank-confirm.png
   :width: 800


Du kommer nu til oversigten over kundensansøgninger, her kan du klikke på vis, og få mere detaljeret overblik.

.. image:: /images/resursbank-list.png
   :width: 800


På vis siden kan du se processen over ansøgningen, i tilfælde af status **Pending**, så afventer vi altså input fra kunden, så har de fået en SMS eller email de skal respondere på.

.. image:: /images/resursbank-show.png
   :width: 800


Når vi sender ansøgningen til ResursBank, så fungere det fuldstændig som hvis du har indtastet den i ResursBank interface. Kunden får nu et sæt automatiske SMS'er samt emails, hvor de kan godkende deres låneansøgning.

Status i OptikPartner fungere fuldstændig som et trafiklys, hvis status er rød er ansøgningen afvist, og du får ikke dine penge.

Er status gul så venter vi input fra kunden og er derfor under behandling.

Er status grøn, så kan du læne dig tilbage pengene er på vej.

Opret på kunden
^^^^^^^^^^^^^^^

Den anden fremgangsmåde er hvis ikke du har et salg på kunden, så kan du gå til kundeoverblikket, og i menuen over deres profil billede vælge knappen **Finansieringsansøgning**.

.. image:: /images/resursbank-profile.png
   :width: 800


Alt det andet fungere fuldstændig som i første gennemgang :)

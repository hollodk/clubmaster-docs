Tidsregistrering
================

Optikpartner gør det let for dig at registrer tiden for dine medarbejdere, så lønninger bliver let som en leg.


Som medarbejder
---------------

Her er først funktionerne som du skal være bekendt med som medarbejder.


Start tidsregistrering
^^^^^^^^^^^^^^^^^^^^^^

Når du vil starte en tidsregistrering, så klikker du på dit navn i øverste højre hjørne, og vælger **Stemple ind**, du ser nu en popup-besked som du bedes bekræfte.

.. image:: /images/timetracking_start.png
   :width: 800


Slut tidsregistrering
^^^^^^^^^^^^^^^^^^^^^

Når din tid er igang, kan du se i toppen af skærmen, hvor mange timer du har været på arbejde.

Klik på tiden i toppen af skærmen, hvorefter du kan afsluttet dagens arbejde.

.. image:: /images/timetracking_end.png
   :width: 800


Som administrator
-----------------

Når du skal til at udbetale løn, så kan du gå ind til **Report > Tidsregistrering**, her får du et overblik over alle dine medarbejdere og deres tidsforbrug.

.. image:: /images/timetracking_report.png
   :width: 800


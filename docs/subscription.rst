Abonnementer
============

For at få overblik over abonnementer i OptikPartner har vi en del forskellige værktøjer til dette


Abonnementsflow
^^^^^^^^^^^^^^^

Flowet for alle abonnementer fungere ens, ligemeget om der er tale om den ene eller anden slags abonnement, men der er nogen administrative ting.

Her er forklaret et normalt abonnementsflow fra oprettelse til kunden måtte af årsage opsige sit abonnement.


Oprettelse
~~~~~~~~~~

1. Kunden opretter et abonnement.
2. Herefter sendes en kontrakt eller betalingslink til kunden via OptikPartner.
3. Kunden godkender online eller i butikken.
4. Kunden kan på samme side tilmelde sine betalingsoplysninger.
5. Herefter kører abonnementstrækninger automatisk.


Ændre abonnement
~~~~~~~~~~~~~~~~

Du kan altid ændre et abonnement, det gælder både:

- Priser
- Fornyelsesdato
- Genbestillingsdato

Det er vigtigt hvis du opretter et nyt abonnement, at slette det gamle, da kunder godt kan have flere abonnementer på samme tid.


Vedligeholdese
~~~~~~~~~~~~~~

Hvis kunden for forældet betalinger.

Her kan der være tale om følgende typiske fejl:

**I tilfælde af en af følgende:**

- Betalingsaftalen er ikke aktiveret, eller forkert aktiveret
- Kundens kort er udløbet

Tag fat i kunden og forklar dem situationen, og gensend et link til oprettelse af betalingsaftalen.


**I tilfælde af følgende:**

- Kunden har ingen penge

Her forsøger OptikPartner hver 2 uge at trække penge fra kunden konto.


Afslutte abonnement
~~~~~~~~~~~~~~~~~~~

Ved afslutning af et abonnement, kan der være ubetalte regninger, du kan enten lade dem ligge, og vi vil opkræve alle fremtidige betalinger.

Ellers kan du ifølge aftale med kunden slette betalingerne.


Typer af abonnement
^^^^^^^^^^^^^^^^^^^

OptikPartner har support for forskellige abonnementsformer, du kan læse om dem herunder.


Linseabonnement
~~~~~~~~~~~~~~~

Linseabonnementer kan indstilles på mange forskellige måder, læs mere om det her.

:doc:`/docs/getting-started/contactlensorder-subscription`

Yderligere har vi også support for **Fuldtids** samt **deltidsabonnementer**, det betyder at dine kunder nemt kan skifte mellem om de ønsker linser hver dag, eller blot hver anden dag.

Her gennemgår vi opsætningen af abonnementspakker.

:doc:`/docs/subscription/subscription-packages`


Brilleleje og andet
~~~~~~~~~~~~~~~~~~~

Brilleleje er simple, da disse ikke har mulighed for genbestillinger, samt priserne ofte er mere simple, så der er mindre at tage stilling til.

Læs mere om brilleleje og andre abonnementsformer her.

:doc:`/docs/subscription/subscription`


Online kontrakt og betaling
^^^^^^^^^^^^^^^^^^^^^^^^^^^

OptikPartner kan gøre rigtige meget automatisk for dig, eksempelvis hvis du ikke gider at ligge inde med en masse papirarbejde, så har vi udviklet vores egen online underskrifter, samt kunderne online kan bekræfte priserne og vilkårerne for dine abonnementer.

:doc:`/docs/subscription/contract`.

Hvis du har dine egne systemer til dette, kan du blot gå videre til næste punkt **Betalingsaftaler**, så holder OptikPartner blot styr på opkrævningerne, mens du har flexibiliteten til at lave kontrakter og bekræftigelser som du ønsker.


Betalingsaftale
^^^^^^^^^^^^^^^

Betalingsaftaler kan bruges når en kunde skal betale på abonnement, og du ikke ønsker at sende en online kontrakt til kunden, eller hvis kundens kort er blevet spærret og de skal tilmelde et nyt kort.

Læs mere om hvordan du opretter betalingsaftale på kunden.

:doc:`/docs/subscription/payment-agreement`.


Overblik og administration
^^^^^^^^^^^^^^^^^^^^^^^^^^

Når du har med abonnementer at gøre, så er det vigtigste at du sikre at pengene kommer ind, og at du kan følge udviklingen af din butik, her kan du læse mere om dette.

:doc:`/docs/subscription/administration`.


Skifte periode
^^^^^^^^^^^^^^^

Læs om dagligdags opgaver ved et abonnement, så som skifte betalingsperiode og ændre priser.

:doc:`/docs/subscription/daily`.


Kalender
========

Opsætning
^^^^^^^^^

Kalenderen i OptikPartner kan sættes op på forskellige måde, klik her og se nogen metoder.

Gå til :doc:`calendar/setting`

Gå til :doc:`calendar/color`


Email notifikationer
^^^^^^^^^^^^^^^^^^^^
Læs mere om OptikPartner og kalender her.

Her er en vejledning til opsætning af email notifikation når I får booking af eksempelvis synsprøve.

- Til venstre i menuen, skal der klikkes på **indstillinger**.
- Derefter under **integration** og derunder vælg **plugins**.

Under plugins skal der vælges **notification email**, som vist på billedet under.

.. image:: /images/calendar-emailnotification.png
   :width: 800

Deri skal skrives den email, som man ønsker at modtage en notifikation på.


Annoncering ved booking
^^^^^^^^^^^^^^^^^^^^^^^

Her er en vejledning til opsætning af annoncering af besked i forbindelse med booking.

- Til venstre i menuen, skal der klikkes på **indstillinger**.
- Derefter under **integration** og derunder vælg **plugins**.

Under plugins skal der vælges **announcement**, som vist på billedet under.

.. image:: /images/calendar-announcement.png
   :width: 800

Deri skal skrives den information som du gerne vil have kunden til at læse.

Facilitet Opsætning
===================

Her kan du læse om nogen specielle metoder du kan opsætte dit booking plugin på din hjemmeside.


Forskellige tidspunkter
~~~~~~~~~~~~~~~~~~~~~~~

Facilitet skema
^^^^^^^^^^^^^^^

I nogle tilfælde giver det mening at du har åbent for synsprøver alle dage, men kun linsekontrol nogen givne dage på ugen.

For at konfigurer specifikke tidspunkter et produkt kan bookes på, skal du knytte et facilitetsskema til produktet med en anden tid.

Først opret et skema som passer med det nye produkt, i tilfælde af åbent blot tirsdag og torsdag mellem 10 og 17, skal du lave et skema med følgende opsætning.

  | [{
  |   "interval": "60",
  |   "sections": [{
  |     "start": "10:00",
  |     "end": "17:00"
  |   }]
  | },
  | {
  |   "days": "monday,wednesday,friday,saturday,sunday",
  |   "sections": [{
  |     "is_closed": true
  |   }]
  | }
  | ]

.. image:: /images/facility-different-times.png
   :width: 800


Facilitet
^^^^^^^^^

Derefter skal du koble produktet på din nuværende kalender, med et andet skema.

Gå derfor først til **Indstillinger > Booking > Facility** og **rediger** den facilitet du ønsker at ændre.

.. image:: /images/facility-allow-product.png
   :width: 800


I højre side kan du se en boks der hedder **Produkter**, vælg her **Tilføj ny**.

Her vælger du først produktet du ønsker at have anden tidsskema på.

Så vælger du det andet skema, og til sidst sikre dig **Type** blot er visible så den bliver vist.

.. image:: /images/facility-allow-product-edit.png
   :width: 800


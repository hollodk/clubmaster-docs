Online Booking
==============

Her kan du se hvordan du indsætter online booking på din hjemmeside.

For at benytte dig af online booking er der et par forskellige ting som skal spille sammen.


Produkter
~~~~~~~~~

Produkterne skal oprettes for at der er noget synligt for kunderne at booke, det kan ex. være synsprøve, linsekontrol osv.

Disse skal i fanen **Booking** have aktiveret **Is online booking** samt **Duration**, som bestemmer hvor længe kunden skal booking i kalenderen.

.. image:: /images/facility-product.png
   :width: 800


Facilitet
~~~~~~~~~

Faciliteter er dine ressourcer, du kan oprette evt. en facilitet per synsprøve rum for at tillade flere samtidige bookings.

.. image:: /images/facility-facility.png
   :width: 800


Facilitet skema
~~~~~~~~~~~~~~~

Disse bestemmer hvor længe samt hvilke dage der er åbent og lukket.

.. image:: /images/facility-facility-schedule.png
   :width: 800


Plugin
~~~~~~

Plugins opretter et element som du kan indsætte på din hjemmeside, som samler alle ovenstående punkter.

.. image:: /images/facility-plugin.png
   :width: 800


Hjemmeside
~~~~~~~~~~

For at indsætte dette booking plugin på din hjemmeside er alt du skal gøre, gå til **Indstillinger > Integration > Plugin**.

Her vælger du blot knappen **hjælp**, her kan du se en iframe som du kan indsætte på din hjemmeside.

I tilfælde det er for teknisk, er der også en tekst som forklare hvad der skal gøres som du kan sende direkte til din udvikler, der nemt kan indsætte den på din side.

.. image:: /images/facility-integration.png
   :width: 800


Hardware
========

Her er en indholdsfortegnelse til alle vores integrationer.

.. toctree::
    :maxdepth: 1

    /docs/hardware/opticon.rst
    /docs/hardware/tracer.rst
    /docs/hardware/verifone.rst
    /docs/hardware/usb.rst
    /docs/hardware/zebra-gx430t.rst


Magtek Cardreader
-----------------

Mangler indhold


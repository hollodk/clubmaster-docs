Pensopay
========

Her er alt hvad du skal bruge for at komme igang med din abonnementsløsning.

For at benytte OptikPartner med abonnementsopkrævning har vi gjort det let for dig, du skal kun ansøge gennem Pensopay, og så hjælper vi med alt der kræves bagved, såsom en indløser aftale med Clearhaus så du nemt får dine penge ned på din bank konto.


Ansøgning
---------

Først gå til linket:

https://apply.pensopay.com/optikpartner

Udfyld siden med dine kontaktinformationer password som du ønsker at bruge.

.. image:: /images/partners-pensopay-signup.png
   :width: 800


Vælg nu på den næste side her **Create Application**, og din ansøgning vil strakt start op.

.. image:: /images/partners-pensopay-create.png
   :width: 800


Udfyld herefter alle felterne som vedrører din forretning.

Du kan altid hive fat i os hvis du har spørgsmål til udfyldelsen af disse informationer

Når du er færdig, så klikker du blot på **Submit This Application**, og din ansøgning er nu igang, det tager oftest 2-3 hverdage før den er godkendt.

.. image:: /images/partners-pensopay-submit.png
   :width: 800




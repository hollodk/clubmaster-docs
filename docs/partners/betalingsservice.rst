Betalingsservice
================

Her er alt hvad du skal bruge for at komme igang med din abonnementsløsning.

Først er det vigtigt at påpeje at der er forskel på PBS og deres BS Web, denne løsning er en standalone løsning, og er ikke noget som kan integreres, det er derfor vigtigt du har et Betalingsservice abonnement hos PBS.


Krav
----

Du skal have en MitID konto som er tilknyttet til dit CVR nummer.


Ansøgning
---------

Først skal du gå til:

https://betalingsservice.mastercard.com/

Her vælger du så **Opret aftale**

.. image:: /images/partners-betalingsservice-first.png
   :width: 800


Herefter skal du nu logge på med MitID som er koblet til din erhvervskonto.

.. image:: /images/partners-betalingsservice-second.png
   :width: 800


Herefter følger du alle 10 sider med dine informationer om din forretning.


Punkter til oprettelsen du skal være opmærksom på:

- Du skal vælge dig selv som dataleverandør
- Du skal vælge delsystem **OptikPartner**
- Du behøver ikke udfylde debitor nummer, men vil få et tilsendt af PBS efter oprettelsen


Sende filer
-----------

Når du har din ansøgning, så skal vi bruge følgende informationer fra dig, for at kunne sende filer for dig.

- Brugernavn til betalingsservice
- Password til betalingsservice
- SSH nøgle som du får i velkomst brev


Mangler SSH nøgle
-----------------

Har du ikke din SSH nøgle, kan du ringe til PBS på 0045 80 81 06 79 og få hjælp til at få den udleveret.


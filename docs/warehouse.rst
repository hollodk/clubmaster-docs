Lager
=====

Læs mere om OptikPartner og lager her.


Hvis du fører lager i din butik, så kan du altid gå ind på menuen **Produkt > Lager**, og se din aktuelle lager værdi som kan bruges til regnskab.

.. image:: /images/warehouse-overview.png
   :width: 800


Udfør lageroptælling
--------------------

Når du skal lave lageroptælling kan du lave 2 forskellige formater **Delvis optælling** og **Fuld optælling**.


Delvis optælling
~~~~~~~~~~~~~~~~

Når du laver en delvisoptælling, så laver vi udelukkende en status over de produkter, du har valgt at rette til, det vil sige:

- hvis du har 50 produkter
- og retter i 5 af dem

Så vil vi blot ændre de pågældende 5 produkter, du har optalt.


Fuld optælling
~~~~~~~~~~~~~~

Omvendt når du laver en fuld optælling, så laver vi en status over samtlige produkter i dit system , og vi nulstiller dermed alle produkter til 0 hvis ikke de er optalt. Det vil sige:

- hvis du har 50 produkter
- og retter i 5 af dem

Så vil vi rette de 5 til hvad du har indtastet, og fjerner alle andre produkter.


Start en optælling
------------------

Når du skal lave en optælling, så går du til **Produkter > Lageroptælling** og klikker på knappen **Start en ny optælling**.

Vælg så om du vil lave en **delvis** eller **fuld optælling**.


.. image:: /images/warehouse-count-start.png
   :width: 800


Optælling
~~~~~~~~~

Du får nu en lang liste over dine produkter, gennemgå nu dine produkter en for en, og indtast hvad du har på lager.

.. image:: /images/warehouse-count-work.png
   :width: 800

Opsummering
~~~~~~~~~~~

Når du har talt disse varer du har i butikken eller som du ønsker. Klik derefter på knappen **Se opgørelse**, og du kan nu se hvordan vi vil rette dine produter til, og hvad din kommende lagerværdi vil blive ændret til.

Hvis du er enig i ovenstående du ser på skærmen, klik derefter på **Afslut og korriger lageroptælling**.

Dine produkter vil nu automatisk have ændret antal og alting stemmer som optalt.

.. image:: /images/warehouse-count-summary.png
   :width: 800

BBPOS WisePOS E
===============

Tilføj terminal
---------------

På din terminal
~~~~~~~~~~~~~~~

På din terminal, **Swipe fra højre side mod venstre** og åben **Settings**.

Koden er **07139**.

Vælg **Generate pairing code**.


I OptikPartner
~~~~~~~~~~~~~~

Gå til **Indstillinger > Ekstern > Stripe Integration**

Vælg **Vis** ud for din stripe konto.

Klik på **Tilføj terminal**.

Indtast nu koden fra din terminal.

Terminalen er nu forbundet til OptikPartner.

Noter nu dit serie nummer som er vist på siden lige efter du har koblet terminalen til.

.. image:: /images/stripe-wisepos-paringcode.png
   :width: 800


Lav til betalingsform
---------------------

Gå til **Indstillinger > Økonomi > Betalingsmetode**.

Vælg her **Opret ny**.

Vælg Type **ClubPAY**.

Under oprettelsen vælg her.

 | Navn på betalingsform, ex. ClubPAY
 | Type **ClubPAY**
 | Vælg din Stripe konto
 | Kontoplan til regnskab
 | Serie nummer fra tidligere i guiden, ex. WSC513111008331

Gem dine ændringer, og terminalen kommer nu automatisk frem i dit kassesystem.

.. image:: /images/stripe-wisepos-create.png
   :width: 800

.. image:: /images/stripe-wisepos-pos.png
   :width: 800


Refundering af betaling
-----------------------

Kommer kunder ind og beder om en refundering, så sker dette ikke via terminalen, gå blot ind i **Indstillinger > Økonomi > Betalingsmetode**.

Find her klappen **log** ud for din nye terminal.

Her vælger du blot at tilbageføre beløbet til kunden.


Tilpas billede
--------------

Du kan selv uploade et nyt valgfrit billede til din terminal for at gøre den helt din egen.

Først, gå til dit Stripe dashboard på https://dashboard.stripe.com.

.. image:: /images/stripe-dashboard.png
   :width: 800


Gå så til Menuen **More > Terminal**.

Klik herefter på linien med din butik, der er ingen knap, klik blot på linien.

.. image:: /images/stripe-location.png
   :width: 800


Klik herefter på **Reading settings** i toppen af din skærm, og klik her på **Upload new**.

.. image:: /images/stripe-settings.png
   :width: 800


En menu åbner sig, opload dit ønsket billede materiale, det skal gerne være i dimentionen:

 | Højde 1280
 | Bredde 720

Og vent så et par minutter, og du har nu dit eget billede på din ClubPAY terminal.


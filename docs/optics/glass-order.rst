Glasbestilling
==============

Her har vi en gennemgang af glasbestillingen i OptikPartner.


Gå til glasbestilling
---------------------

Først skal du finde din kunde, søg kunden frem på normal vis.

Klik derefter på **Journal / Noter** i øverste højre hjørne, og herfra gå til **Opret glasbestilling**.

.. image:: /images/optic_glassorder_customer.png
   :width: 800


Recept og stel
--------------

Du er nu igang med din glasbestilling.

I øverste felter har du mulighed for at overføre konklusioner fra tidligere journaler, vælg den som passer til netop denne glasbestilling.

Husk også at udfylde **PD** samt **højde**.

Du kan også her indsætte et stel, så det bliver nemt for dig at lave et færdigt arbejdskort til kunden.

Ydermere kan du nederst på siden vælge om det er en ny **bestilling**, eller om det er en **genbestilling**, resultatet af dine valg viser vi i vores rapporter, så du hurtigt kan få et overblik over hvor mange kunder der kommer tilbage, og evt. er utilfredse.

Se disse i **Rapport > Overblik**.

.. image:: /images/optic_glassorder_prescription.png
   :width: 800


Valg af glas
------------

Derefter i næste fane skal du vælge dine glas, brug søgefunktionen til højre for at finde noget som hurtigt passer til kundens ønsker.

Når du har valgt et glas, kommer du længere ned på siden hvor du kan vælge coating og andre optioner for yderligere tilpasning af glasset.

.. image:: /images/optic_glassorder_glass.png
   :width: 800


Tracing
-------

Hvis du ønsker fjernslibning af dine glas, kan du gå til **Facon/form**.

Vælg her om du ønsker glasset fjernslebet, eller om du ikke ønsker dette men blot modtage et råglas.

Nogle leverandører har leveret en fuld trace database til os for du kan spare tid, du kan vælge menuen i menuen **Forrige trace**, og vælge dropdown menuen, **Database**.

Skal du genbruge et trace fra tidligere, kan du også se historikken over dine tidligere traces, hvis du skal lave en genbestilling.

.. image:: /images/optic_glassorder_trace.png
   :width: 800


Individuelt
-----------

Nogle glas kan tilpasses i større tilpasningsgrad i form af Stelvinkel, progressionslængde og topmåling.

Hvis dette er muligt vil en menu **Individuel** være synlig i toppen, og her kan du sætte disse indstillinger.

Vær opmærksom på ikke alle glas har mulighed for dette, dette styres fra glasfabrikkerne.

.. image:: /images/optic_glassorder_individuel.png
   :width: 800


Tilbehør/Opsummering
--------------------

Når du har valgt alle dine optioner vedr. glas og stel, gå derefter til sidste fane.

Her kan du lave de sidste tilpasninger, evt. lægge andre produkter på arbejdskortet, eller give rabatter eller andet som er specifikt for denne bestilling.

Når du er færdig, klik derefter på **Gem og opret arbejdskort**.

.. image:: /images/optic_glassorder_summary.png
   :width: 800


Arbejdskort
-----------

Du ser her alle detaljer vedr. din glasbestilling, hvilket er disse data vi sender til glasfabrikken så du får dine glas præcis som du ønsker.

Her skal du være opmærksom på 2 ting.

Første step, er at læse ned over siden og kontrollere alting er i orden, klik herefter **Udfør** ud over processbaren **Klar**, så sender vi bestillingen videre til fabrikken.

Når du modtager glasset, klikker du så på knappen **Udfør** ud for modtaget, så har du styr på din historik.

Når det hele er klar til kunden, klikker du på **Udfør** ud for afsluttet, og vi sender en notifikation til din kunde om glasset er klar og de kan komme og hente det.

Til slut, kan du bruge **Udleveret**, når kunden har været i butikken og hente sine glas, og så er hele processen færdig.

.. image:: /images/optic_glassorder_workcard.png
   :width: 800


Til kassen
----------

Når kunden skal betale, brug da øverste knap **Ekspedition**.

Her kan du vælge at overføre glasbestillingen til din ekspedition, hvorefter alt kører som et helt almindeligt salg.

.. image:: /images/optic_glassorder_sale.png
   :width: 800


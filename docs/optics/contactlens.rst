Kontaktlinser
=============

Her er nogen forskellige forklaringer til kontaktlinsebestilling i OptikPartner.


Gå til linsebestilling
----------------------

Først skal du finde din kunde, søg kunden frem på normal vis.

Klik derefter på **Journal / Noter** i øverste højre hjørne, og herfra gå til **Opret kontaktlinsebestilling**.

.. image:: /images/optic_glassorder_customer.png
   :width: 800


Vælg recept
-----------

Vi overføre automatisk fra den seneste journal på kunden, ønsker du at bruge en anden, kan du klikke på knappen **Importer fra tidligere**, og du ser en liste med alle tidligere journaler.

.. image:: /images/optic_lenses_prescription.png
   :width: 800


Vælg linser
-----------

Derefter kan du vælge blandt de forskellige leverandører i søgemenuen i venstre side.

Du ser resultaterne lige under den overførste konklusion, vælg blot de linse du ønsker at bestille.

Skal det være forskel på højre og venstre linse, kan du bruge **Venstre** knappen til at lave den forskel.

.. image:: /images/optic_lenses_lenses.png
   :width: 800


Tilpas styrker
--------------

Når du har valgt en linse, bliver du først længere ned på siden, hvor du kan vælge antal samt kurve, diameter, styrker og andre parametre for netop denne linse.

.. image:: /images/optic_lenses_order.png
   :width: 800


Leveringsmetode
---------------

I boksen, **Salg og abonnement** kan du vælge om du ønsker at bestille fra leverandøren, eller om du tager fra eget lager.

Lige under i boksen **Levering** kan du vælge om du ønsker levering til butikken, eller om linserne skal sendes hjem til kunden.

Udfyld i så tilfælde kundens adresseoplysninger samt telefon nummer.

.. image:: /images/optic_lenses_delivery.png
   :width: 800


Arbejdskort
-----------

Du ser her alle detaljer vedr. din linsebestilling, hvilket er disse data vi sender til leverandøren.

Her skal du være opmærksom på 2 ting.

Første step, er at læse ned over siden og kontrollere alting er i orden, klik herefter **Udfør** ud over processbaren **Klar**, så sender vi bestillingen videre til leverandøren.

Når du modtager linserne, klikker du så på knappen **Udfør** ud for modtaget, så har du styr på din historik.

Når det hele er klar til kunden, klikker du på **Udfør** ud for afsluttet, og vi sender en notifikation til din kunde om linserne er klar og de kan komme og hente dem.

Til slut, kan du bruge **Udleveret**, når kunden har været i butikken og hente sine linser, og så er hele processen færdig.

.. image:: /images/optic_lenses_workcard.png
   :width: 800


Til kassen
----------

Når kunden skal betale, brug da øverste knap **Ekspedition**.

Her kan du vælge at overføre linsebestillingen til din ekspedition, hvorefter alt kører som et helt almindeligt salg.

.. image:: /images/optic_lenses_sale.png
   :width: 800


Opret linse abonnement
----------------------

På arbejdskortet kan du klikke på knappen **Admin > Opret abonnement**.

Her kan du nemt tilpasse et nyt abonnement til kunden, når du er færdig klikker du blot på knappen i bunden **Opret abonnement**.

.. image:: /images/optic_lenses_subscription.png
   :width: 800


Noah 4 System
=============

Noah 4 er et audio system til hørebranchen, som vi kan integrer med i OptikPartner.

Vi kan sende patient data og hente det tilbage igen i OptikPartner og vise høreprøver og andre relevante ting.


Installation af Noah 4 System
-----------------------------

For at komme igang med Noah 4 skal du først installere det på din computer, vi har vedhæftet en guide her.


Noah 4
~~~~~~

Install Noah 4 System fra himsa's hjemmeside.

https://www.himsa.com/himsa_download/noah-system-download/

**Vent med at åbne til senere step**


WSI licens
~~~~~~~~~~

Installer WSI licens som du kan få udleveret af OptikPartner


WSI program
~~~~~~~~~~~

Download nu WSI programmet som ligger på himsa's hjemmeside i et brugerbeskyttet område, typisk kan denne udleveres af OptikPartner hvis den mangler.

Gå til himsa.com vælg i menuen Members > Web-Service-Interface WSI > WSI SDK

Her kan du nu downloade WSI pakken som passer til din Noah 4 installation, typisk den nyeste.

**Typisk herefter skal du genstarte**


Start Noah 4
~~~~~~~~~~~~

Åben nu Noah 4, og programmet vil nu gøre 2 ting:

- Sige at WSI er installeret og genstarte
- Bede dig om at indtaste din licens nøgle du har fået af OptikPartner


Færdig
~~~~~~

Du er nu færdig med installationen af Noah 4, og alt er installeret korrekt.

Du kan få en final validering om det hele er sat korrekt op ved at gå i Windows command prompt og skrive:

| netstat -ant | findstr 8080


Installation af LiloApp
-----------------------

Nu skal du installere LiloApp, og konfigurer den til at supporte Noah, følg guides i LiloApp for dette.

Kontroller ved at gå til en kunde i OptikPartner, gå til overførsler side.

Tryk overfør til Noah, og Noah 4 skulle nu starte op med den angivne patient.


Udfør prøver i Noah 4
---------------------

Udfør nu dine tests og andet i Noah 4, og klik gem.


Kontrol OptikPartner
--------------------

Så snart du har gemt dem i Noah vil de indenfor få sekunder også være synlige i OptikPartner, du vil kunne se den lille LiloApp kommer med en notifikation om at den har modtaget en læsning.

Tilbage i OptikPartner på kunden kan du i kundens tidslinie se et audiogram på kunden.


Daglig arbejde i Noah 4
-----------------------

Her er et komplet beskrevet workflow med OptikParner og Noah 4


Find kunde i OptikPartner
~~~~~~~~~~~~~~~~~~~~~~~~~

Find kunden du vil overføre til Noah 4, find her overførselsknappen lige over adressen på kunden.

.. image:: /images/noah/noah-customer-view.png
   :width: 800


Klik på overfør til Noah
~~~~~~~~~~~~~~~~~~~~~~~~

Klik nu på knappen overfør til Noah

.. image:: /images/noah/noah-transfer-patient.png
   :width: 800

Udfør dit arbejde i Noah
~~~~~~~~~~~~~~~~~~~~~~~~

Kunden starter nu automatisk op i Noah, udfør dit arbejde og gem når det passer.

.. image:: /images/noah/noah-patient-transferred.png
   :width: 800

.. image:: /images/noah/noah-save-journal.png
   :width: 800


Overført til OptikPartner
~~~~~~~~~~~~~~~~~~~~~~~~~

Data bliver nu automatisk overført til OptikPartner, du kan se et lille popup i Windows når Noah har sendt os data, tager typisk 5-10 sekunder efter du har klikket gem.

.. image:: /images/noah/noah-view-timeline.png
   :width: 800

.. image:: /images/noah/noah-journal-view.png
   :width: 800


LiloApp
=======

LiloApp er et lille program som kører på din enten windows, mac eller linux computer.

Det står for at sende data fra dit optiske udstyr til din OptikPartner platform.


Installation
------------

Download programmet her https://www.optikpartner.dk/archive/liloapp/lilo-app_latest.exe

Installer programmet som helt normalt.

Når LiloApp er installeret vil du få en lille popup på computeren der siger at programmet er klar.

Du vil nu se et lille ikon i din system bar ved klokken, klik på dette med højre musetast for at åbne en menu.

.. image:: /images/lilo-app-menu.png
   :width: 800


Menu
----

Her er en beskrivelse af menu enhederne

**Open web panel**
Gå til konfigurationsprogrammet, det åbner et browser vindue hvor du kan indstille hvordan LiloApp'en skal fungere.

**View logs**
Viser logs over hvordan programmet kører, du kan altid gå ind i denne fil, og se om programmet gør hvad du forventer.

**View output**
Går til mappen som indeholder alt indhold som LiloApp sender til cloud løsningen.

**Restart**
Genstart programmet hvis du har rettet til i konfigurationen.

**Quit app**
Luk programmet, i tilfælde af du har rettet indstillinger.


Indstillinger
-------------

LiloApp bliver styret fra et web interface, du tilgår blot http://localhost:56789/page/config fra computeren LiloApp er installeret på.

.. image:: /images/lilo-app-web.png
   :width: 800

Følg blot guides som ligger tilgængelig online.


Forbind til OptikPartner
------------------------

For at forbinde din PC til OptikPartner, gå da til **Indstilliner > System > LiloConnect**

Åben din konfiguration fra tidligere, og kopiere **TOKEN** ind i oprettelsen af en ny LiloConnect, og husk at vælg en lokation.


.. image:: /images/lilo-app-connect.png
   :width: 800

Det er vigtigt at du connecter din LiloApp med OptikPartner, for at vi ved hvilket butik du er tilknyttet.


Synsprøve
=========

I OptikPartner kan du nemt lave dine synsprøver.

Her gennemgår vi et par forskellige muligheder.


Sende synsprøve til en kunde
----------------------------

Hvis en kunde ønsker sin synsprøve udprintet eller tilsendt, så er den letteste måde at printe den som PDF, og enten skrive den ud eller vedhæfte den via email.


Først, gå til den ønsket synsprøve du ønsker at udlevere til kunden.

Når du er inde på journalen klik i toppen på **Flere** knappen, som giver flere optioner.

Klik herefter på knappen **Print**

Ønsker kunden en udskrift af synsprøven, kan du nu skrive den ud på jeres normale printer hvis du ønsker.

Ønsker kunden den tilsendt via email, så vælger du blot den printer som hedder **Gem som PDF**, herefter gemmer du filen på computeren.

Du kan nu vedhæfte den som email i dit normale email program du sender til kunden.

.. image:: /images/eye-exam-print.png
   :width: 800


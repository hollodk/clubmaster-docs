Kontobevægelser
===============

Her er en indholdelsefortegnelse til alle vores integrationer.


Gensynkroniser posteringer
--------------------------

Engang i mellem vil der være noget miskonfiguration i posteringerne. Det kan være man ikke har fået sat konto planerne op for sine salg.

Det kan resultere i at regnskabet ikke bliver overført komplet.

Derfor skal du fra tid til anden følge med i overførselsloggen som du finder i **Indstillinger > System > Logs > Account**.

Sorter gerne på alle **FEJL**, for at få et hurtigt overblik, bemærk fejl beskeden på hver linie, som fortæller hvad der er galt.

Når du klikker på den blå knap **Ledgers**, går du videre til posteringsoverblikket.

.. image:: /images/account-logs-account.png
   :width: 800


Hvis der er fejl på posteringen, så ser du nu det aktuelle salg, her har du 2 muligheder.

**Rediger**, her kan du redigere konto plan, hvis noget har været sat forkert op.

**Resynkroniser**, her kan du overføre posteringen på ny til dit regnskabsprogram, du har en række forskellige muligheder.

.. image:: /images/account-logs-resync.png
   :width: 800


Hele salget
~~~~~~~~~~~

Den metode overføre både salg og betaling forfra.


Salgsenheder
~~~~~~~~~~~~

Vil blot overføre salgslinierne og ikke overføre betalingerne.


Betalingslinier
~~~~~~~~~~~~~~~

Her overføre vi bot betalingerne igen, men lader salget være urørt.


Postering
~~~~~~~~~

Her overføre vi blot den enkelte postering, vær opmærksom på at hvis din synkronisering er sat til at overføre som faktura, vil dette ikke fungere, så tag evt. en snak med support hvis du er i tvivl.

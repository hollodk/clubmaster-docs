Kontaktlinse abonnement
=======================

Ting du skal vide
-----------------

Her får du en fuld gennemgang af opsætning af linseabonnement i OptikPartner.

Husk at det er vigtigt, når du opretter produkt grupper, er det vigtigt at disse også eksistere i dit regnskabsprogram hvis du har faktura synkronisering med regnskab, ellers vil moms ydelser som oftest gå galt.


Beregning af priser
~~~~~~~~~~~~~~~~~~~

Når du sælger linser på abonnement, på forskellige måder.

**Den lette løsning**

Som udgangspunkt bliver hentet ved at lægge antallet af linser sammen med prisen i produkt kartoteket.

Du indtaster selv antal linser og beløb.


**Med klinisk service**

Du kan også gå til **Indstillinger > Produkter > Abonnementpakker**, og her kan du abonnementspakker kan være:

- Fuldtidsabonnement
- Deltidsabonnement

Her vælger du et program i **Tilføjelse**, som er det kliniske produkt.

Når du nu vælger nogle linser, så bliver abonnementet splittet op som dit abonnement er sat op, en del til linser og en del til tilføjelses produktet.

.. image:: /images/contact-lens-subscription-packages.png
   :width: 800


**Skræddersyet priser**

Når du har oprettet abonnementspakkerne kan du gå videre ind og konfigurer specifikke priser til disse pakker. Gå til **Indstillinger > Produkter > Abonnementspakker** og ud for hver pakke kan du se en knap **Produkter**.

Her kan du sætte korrekt antal og priser fuldstændig som du ønsker.

.. image:: /images/contact-lens-subscription-package-products.png
   :width: 800


Oprette produkt gruppe
----------------------

Sikre dig at du har 2 produkt grupper parat, en skal være til produkter med moms og en skal være uden moms. Det er vigtigt disse er oprettet i dit regnskabsprogram, og moms koder er opsat heri ligeledes.

Du kan lave det simpelt, hvis du ikke ønskere en avanceret kontoplan i dit regnskab.

Kontaktlinser kan gå til **Varer incl. moms**

Linsehonorar kan gå til **Varer ex. moms**


Opret linsehonorar
------------------

Du skal have sikret dig at du kan få en momsfri ydelse, det gør du ved at lave et linsehonorar produkt i dit katalog.

Sikre dig at det får en produkt gruppe som er momsfri i dit regnskab, som vist ovenfor.


Optisk konfiguration
--------------------

Før du går igang er det vigtigt at gå igennem punkterne i **Indstillinger > Optik > Optisk konfiguration**.

Her skal du indstille **Standard moms** hvis ikke dette er gjort.

Du skal også indstille **Standard produkt gruppe** for kontaktlinser, for at undgå for meget manuelt konfiguration.


Impotering af linser
--------------------

Kontakt OptikPartner for at få impoteret linser til de rette leverandører.


Opsætning af priser
-------------------

Derefter gå igennem linserne, og sikre dig at vores vejledende priser stemmer overens med dine udsalgspriser, ellers kan du nemt rette dem til.


Opsætning af abonnementspakker
------------------------------

Hvis du ønsker at køre med fuldtid samt deltidsabonnementer, kan du oprette disse i OptikPartner under **Indstillinger > Produkter > Abonnementspakker**.

Klik først på **Opret ny**, det er vigtigt at du vælger et **Additional produkt**, da dette vil blive din momsfri ydelse, vælg så enten en procentsats eller en option i listen **Rules** for gængse værdier som:

Spheric 100 kr
Multifocal 125 kr
Toric 150 kr

.. image:: /images/configuration-lens-subscription-package.png
   :width: 800


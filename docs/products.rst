Produkter
=========

Læs mere om OptikPartner og produkter her.

.. toctree::
    :maxdepth: 1

    /docs/products/glass.rst


Brug filteret til at finde specifikke produkter.

Eksempelvis hvis du gerne vil finde produkter, som endnu ikke har fået lavet kostpris, kan du gå til **Produkter**, og søge på **cost:0**.

Så er det nemt at rette produkterne til.

.. image:: /images/product-filter-search.png
   :width: 800


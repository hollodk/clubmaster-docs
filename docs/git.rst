Git Windows
===========

Lav ændringer i Git
-------------------

Her er en guide til hvordan du kommer igang med at rette OptikPartners dokumentation, det hele kræver selvfølgelig du har rettigheder.

Der er forskellige måder at lave ændringer i vores dokumentation, her er en måde på din egen computer, og den anden måde er direkte i browseren.


Brug gitlab Web IDE
-------------------


Åben Web IDE
^^^^^^^^^^^^

Først gå blot til https://gitlab.com/hollodk/clubmaster-docs, find og klik på knappen Web IDE.

.. image:: /images/git-webide.png
   :width: 800


Rediger filer
^^^^^^^^^^^^^

Først går du i mappen docs/ her ser du alle filer med dokumentation, ret i den fil du ønsker i tekst vinduet.

.. image:: /images/git-webide-edit.png
   :width: 800


Commit ændringer
^^^^^^^^^^^^^^^^

Når du er færdig, så klikker du på den farvede knap i bunden "Commit".

Husk at vælg, "Commit to main branch", det andet vil også fungere, men det springer blot nogen steps over.

.. image:: /images/git-webide-commit.png
   :width: 800


Brug gitkraken på din computer
------------------------------

Du kan også downloade et program på din computer, så du ikke er afhængige af internet, det kan være godt til ferier :)


Download
^^^^^^^^

Først download en windows client på https://www.gitkraken.com/.

.. image:: /images/git-download.png
   :width: 800


Opret ny konto
^^^^^^^^^^^^^^

Indtast navn og email, og lav efterfølgende en profile.

.. image:: /images/git-account-new.png
   :width: 800


Clone a Repo
^^^^^^^^^^^^

Vælg gitlab, og forbind derefter med dine informationer.

.. image:: /images/git-clone.png
   :width: 800


Rediger file
^^^^^^^^^^^^

Brug nu din favorit editor til at rette filerne, vi har her blot vist notepad som eksempel.

.. image:: /images/git-file-edit.png
   :width: 800


Åben GitKraken
^^^^^^^^^^^^^^

Nu har du lavet nogen ændringer i dine filer, og de skal derfor gemmes til vores cloud løsning.

Først har du her et overblik over historikken af hvad der er sket i mappen.

Hvis ikonet "Save Changes" er grønt til højre, så har du lavet ændringer som er klar til at blive gemt.

.. image:: /images/git-changes.png
   :width: 800


Commit ændringer
^^^^^^^^^^^^^^^^

Klik nu på disse filer du ønsker at gemme, og tilføj en passende subject og comment, så folk efter dig kan se hvad der er lavet.

.. image:: /images/git-commit.png
   :width: 800


Push ændringer
^^^^^^^^^^^^^^

Når du er færdig og tilfreds med dit arbejde, så skal du "pushe", dine ændringer til gitlab, så bliver ændringerne tilgængelig for alle.

Du skal blot indtaste dit brugernavn og password til gitlab for at komme videre.

.. image:: /images/git-push.png
   :width: 800


Verify
^^^^^^

Når du er færdig, kan du gå ind på vores fælles repository og se dine ændringer med dit navn.

https://gitlab.com/hollodk/clubmaster-docs

.. image:: /images/git-verify.png
   :width: 800


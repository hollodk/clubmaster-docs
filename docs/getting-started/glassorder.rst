Glasbestilling
==============

Når du skal oprette en glasbestilling, skal du igen være på vedkommendes kundekort, klikke på "Journal/noter" og denne gang vælge “Opret glasbestilling”.

Alternativt kan du også, når du er færdig med synsprøven og har klikket “Gem og luk journal”, vælge knappen “Gå til glasbestilling” i toppen af vinduet.

.. image:: /images/getting-started/glass-start.png
   :width: 800


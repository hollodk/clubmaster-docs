Sælge glas eller linser
=======================

Når du skal gennemføre et hvilket som helst salg, kan du selvfølgelig gøre det direkte i ekspeditionen, som findes i menuen i højre side.
Men når du sælger glas eller linser, er det endnu lettere at trække hele bestillingen over i ekspeditionen, så du slipper for at indtaste produkterne manuelt.
Det gøres ved at gå til kundens kundekort og vælge menuen "Opgaver", hvorefter du kan se en liste over alle glas/linsebestillinger, der er knyttet til kunden.
Når du klikker dig ind på den pågældende opgave, kan du i toppen se en række blå knapper, hvor du skal trykke på "Ekspedition".


.. image:: /images/getting-started/task-glass-show.png
   :width: 800


Herefter kan du vælge at overføre både produkter og eventuelle kommentarer fra opgaven til et salg i ekspeditionen.

.. image:: /images/getting-started/task-sale-start.png
   :width: 800


Klik på “Opret salg” og du går nu videre til ekspeditionen.

.. image:: /images/getting-started/pos-cart.png
   :width: 800


Gennemfør nu betalingen som “Almindelig salg”


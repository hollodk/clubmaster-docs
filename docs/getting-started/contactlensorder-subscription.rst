Linseabonnement
===============

Når en kunde skal have oprettet et kontaktlinseabonnement, skal du først have lavet en linsebestilling, som du bruger som udgangspunkt for selve abonnementet.

Når du har gennemført din linsebestilling og er på selve arbejdskortet, kan du i toppen vælge **Opret abonnement**.


Oprette linsebestilling
^^^^^^^^^^^^^^^^^^^^^^^

Før du starter, opret en helt almindelig kontaktlinsebestilling, så har du også samtidig journalført hvilke linser kunden benytter sig af.

Hvis du har linserne liggende, husk at marker **Tag fra eget lager**, ellers kan du også her i samme omgang bestille først levering af linserne.

.. image:: /images/getting-started/task-lens-show.png
   :width: 800

Er du i tvivl, kan du læse her om linsebestilling.

:doc:`/docs/getting-started/contactlensorder`


Oprette abonnement
^^^^^^^^^^^^^^^^^^

Når linsen er journalført går du blot videre til dit arbejdskort.

Herfra kan du i toppen klikke på knappen **Opret abonnement**, og du vil nu se en menu folde sig ud.

Gennemgå alle felterne, her kan du skræddersy kundens abonnement, vælg eksempelvis:

- Fuldtids eller deltidsabonnement
- Giv kunden en evt. rabat
- Ved automatisk bestilling, indtast hvor længe for fornyelsen du vil bestille linserne

Når du har udfyldt alle felter, kontroller prisen i bunden, og klik på **Opret abonnement**.

Stemmer priserne ikke, kan du enten finde produktet i varekartoteket, eller hvis du bruger abonnementspakker skal du finde linsen under **Abonnementspakker**.

.. image:: /images/getting-started/subscription-task-new.png
   :width: 800


Kontrakt og betaling
^^^^^^^^^^^^^^^^^^^^

Hvis du benytter dig af online kontrakter, gå da til fanen **Kontrakt**.

Først har du 3 knappen i toppen, **Send email**, **Online version** og **Alle kontrakter**.

**Online version**, kan du bruge hvis du har kunden i butikken, og de vil udfylde deres kontrakt i butikken.

**Send email**, sender en email til kunden med denne online kontrakt i tilfælde af de gerne vil læse vilkår igennem hjemme i ro og mag.

.. image:: /images/subscription-contract-email.png
   :width: 800

Når du er på **Online version**, kan du se i øverste højre hjørne, der er der en knap som hedder **Underskriv kontrakt**, her kan kunden enten via en tablet i butikken skrive under, ellers kan de gøre det hjemme hos sig selv med deres egen computer.

Nederst i højre side kan kunden acceptere butikkens handelsvilkår og abonnementsvilkår, for derefter at gå til online betaling.

Læs mere om vores online kontrakter her.

:doc:`/docs/subscription/contract`.


.. image:: /images/subscription-contract-customer.png
   :width: 800

.. image:: /images/subscription-contract-sign.png
   :width: 800


Betalingsaftale
^^^^^^^^^^^^^^^

Når abonnementet er oprettet, kan du tilkoble en betalingsaftale, som gør at kundens betalinger automatisk bliver trukket hver måned.

Læs mere om betalingsaftaler her.

:doc:`/docs/subscription/payment-agreement`.

.. image:: /images/subscription-payment-agreement.png
   :width: 800


Tilpas abonnement
^^^^^^^^^^^^^^^^^

Abonnementet er nu oprettet, og du kan følge trækningsdatoer, leveringsdatoer, alle løbende trækninger m.v. på abonnementssiden.

Fra fanen **Rediger**, kan du sætte en masse forskellige parametre, du kan sætte næste bestillingsdato samt du kan sætte næste trækningsdato i tilfælde af kunden skal have sat sit abonnement på pause.

I tilfælde af kunden altid ønsker at betale først eller sidst på måneden, kan du også vælge feltet **Betalingsbetingelser**, her kan du sætte **First day of month** eller **Last day of month**, så vil abonnementet altid tilpasse sig denne dato.

.. image:: /images/subscription-edit.png
   :width: 800


Priser
~~~~~~

Hvis du skal redigere prisen kunden betaler for sine linser, så er det nederst på **Rediger** siden, her kan du ændre priserne som du har lyst.

Hvis du ønsker at kunden altid skal følge dine liste priser og automatisk justere i forhold til dine nuværende priser, så kan du bruge den lille knap **Sæt til standard priser**, så følger abonnementet automatisk dine listepriser når du ændre disse.

.. image:: /images/subscription-edit-price.png
   :width: 800

Hvis du bruger abonnementspakker, så kan du også se der er en lille menu som hedder **Skift pakke**, her kan du altid skifte mellem dine forskellige pakker.

Når du vælger den, så tilpasser abonnementet sig automatisk antallet af linser, samt prisen bliver også automatisk tilpasset den nye pakke kunden har valgt.


Rediger linser
~~~~~~~~~~~~~~

I tilfælde af kunden skal have nye styrker på sine linser, så gør du nemt dette ved at klikke på knappen **Rediger linse** nederst på **Rediger** fanen.

Så kommer du til linsevalgssiden, her kan du skifte rundt i hvilken styrke kunden skal have på sine linser.

Derefter klikker du blot på knappen **Gem og fortsæt senere**, og de nye styrker er nu koblet til kundens næste automatiske genbestilling.

.. image:: /images/subscription-lens-edit.png
   :width: 800


Afslut abonnement
^^^^^^^^^^^^^^^^^

Læs mere her hvordan du afslutter en kundes abonnement bedst muligt.

:doc:`/docs/subscription/cancel`.


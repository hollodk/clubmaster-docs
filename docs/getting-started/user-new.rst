Ny kunde
========

Du kan oprette nye kunder tre forskellige steder via platformen, alt efter hvad du er i gang med.


Ny kunde via kalenderen
-----------------------

- Først, gå til kalenderen via menuen i højre side.
- Marker nu et tidspunkt, kunden ønsker en ny aftale
- Klik på knappen i højre side “Tilføj ny kunde”.

.. image:: /images/getting-started/user-new-appointment.png
   :width: 800

- Udfyld felterne for den nye kunde

.. image:: /images/getting-started/user-new-appointment-details.png
   :width: 800


Ny kunde via ekspedition
------------------------

Hvis du er igang med et salg til en kunde, som endnu ikke eksisterer i platformen, og som derfor skal oprettes.

- Gå til ekspeditionen via menuen i højre side
- I feltet, hvor du søger efter en kunde, søg nu efter kundens navn.
- Kommer der ingen resultater, ser du knappen “Tilføj en kunde”.

.. image:: /images/getting-started/pos-user-search-add.png
   :width: 800


- Klik på knappen, og du kan nu udfylde alle felterne på kunden.

.. image:: /images/getting-started/pos-user-search-details.png
   :width: 800


Ny kunde
--------

Hvis du blot skal have registreret kundens informationer uden salg eller bookinger, kan du sagtens gøre det via kundemenuen.

- Gå til “Kunde” i hovedmenuen i højre side.
- Klik nu på “Opret kunde” i undermenuen.

.. image:: /images/getting-started/user-index.png
   :width: 800

- Du kan nu udfylde informationerne på din kunde.

.. image:: /images/getting-started/user-new.png
   :width: 800


Kreditering
===========

En gang imellem sker det, at man får lavet en forkert indtastning, eller at kunden ikke ønsker sin vare alligevel, og man derfor må igennem en kreditering af salget.

Det er vigtigt at bruge den rette krediteringsform, da kasseapparatet ellers ikke stemmer.

Først gå til salgshistorikken via “Rapporter > Salgshistorik” eller “Ekspedition > Salgshistorik”.
Find herefter salget ved brug af søgefunktionerne i toppen.
Når du har fundet salget, klikker du på “Vis” ud for salget, og vælger herefter den gule knap “Krediter”.

.. image:: /images/getting-started/pos-sale-show.png
   :width: 800


Du er nu i ekspeditionen med en tydelig marketing af, at det er en refundering med den gule tekst, klik her på knappen “Refund”.

.. image:: /images/getting-started/pos-cart-refund.png
   :width: 800


Du kommer nu til tilbagebetalingssiden. Læg mærke til nederst på siden, hvordan det oprindelige salg er gennemført, det vil typisk være den samme betalingsform, du tilbagefører med. Klik herefter på den ønskede betalingsform, og gennemfør refunderingen.

.. image:: /images/getting-started/pos-refund.png
   :width: 800


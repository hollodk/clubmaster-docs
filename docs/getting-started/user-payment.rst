Kundeindbetalinger
==================

OptikPartner holder godt styr på kundernes balance, det vil sige, hvis kunden ikke betaler med det samme, kan du lægge salget på kundens konto, og herefter holder OptkPartner styr på, hvilke kunder, der skylder dig penge.

Du kan også bruge funktionen til kunder, som skal betale på forskud eller lægge depositum.

Se et overblik over skyldige beløb via “Overblik” i hovedmenuen, se øverste bjælke i højre side “Tilgodehavende”.

.. image:: /images/getting-started/dashboard.png
   :width: 800


Ved indbetaling af gæld, skal du gøre følgende.

Gå til kundens profil, og klik på fanen “Salg”.

.. image:: /images/getting-started/user-sale-index.png
   :width: 800


Hvis kunden har gæld, kan du klikke på knappen “Betal gæld”. Vælg herefter “Gå til kassen” for at gennemføre betalingen i ekspeditionen. Hvis du skal håndtere en bankindbetaling, kan du i stedet vælge “Manuel” for blot at registrere indbetalingen uden at gå til kasseapparatet .

.. image:: /images/getting-started/user-payment-start.png
   :width: 800


Indtast beløbet, som kunden har indbetalt i boksen øverst til højre, og klik på ønsket betalingsform for at registrer beløbet.

.. image:: /images/getting-started/pos-payment-split.png
   :width: 800


Valider, at kundens balance er tilpasset, ved at gå til kundens profil, og gå til salgsfanen igen.

.. image:: /images/getting-started/user-sale-index-after.png
   :width: 800


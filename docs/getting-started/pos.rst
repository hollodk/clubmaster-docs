Ekspedition
===========

Alle salg kører igennem ekspeditionen, hvor du kan føje en kunde til salget, tilvælge produkter og vælges betalingsmetode eller salget kan placeres på konto til senere betaling, og så holder OptikPartner styr på både evt. gæld og overførsel til regnskab.

Almindeligt salg
----------------
Når du ønsker at sælge en almindelig vare, som ikke er en glasbestilling, kan du gå direkte til menuen “Ekspedition” via hovedmenuen.

Find varerne, du ønsker at lægge i kurven, ved at udfylde søgefunktionen til højre eller blandt de oftest solgte produkter i quick menuen nedenfor.

.. image:: /images/getting-started/pos-cart-add.png
   :width: 800


Rediger et produkt, eller giv en rabat ved at klikke på produktet i højre side af skærmen med produktets navn.

.. image:: /images/getting-started/pos-item-update.png
   :width: 800


Tilføj en kunde til salget, ved at søge i boksen ovenfor produkterne i kurven

.. image:: /images/getting-started/pos-user-search.png
   :width: 800


Gå herefter til betaling ved knappen nederst “Betal (PRIS)”. Her kan du vælge den betalingsform, som kunden ønsker at betale med.

.. image:: /images/getting-started/pos-pay.png
   :width: 800


Derefter er salget færdigt, og der kan printes en kvittering, hvis ønsket. Ellers kan der klikkes på “Vis salgsdetaljer”, hvis kunden ønsker at få tilsendt faktura via email.

.. image:: /images/getting-started/pos-sale-completed.png
   :width: 800


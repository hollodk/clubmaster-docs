Linsebestilling
===============

Når du skal have oprettet en glasbestilling, kan du gøre det på samme måde som ved oprettelse af synsprøve. Du skal blot vælge “Kontaktlinsebestilling” i stedet for “Synsprøve”.

Alternativt kan du også, når du er færdig med synsprøven og har klikket “Gem og arkiver journal”, vælge knappen “Gå til kontaktlinsebestilling” i toppen af skærmen.

.. image:: /images/getting-started/user-contactlens-order.png
   :width: 800


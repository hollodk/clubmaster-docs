Synsprøve
=========

For at foretage en synsprøve, skal du fremsøge den pågældende kunde i systemet.
På dennes kundekort kan du i højre hjørne klikke på menuknappen “Journal / Noter”, hvorefter du kan vælge “Ny synsprøve”.


.. image:: /images/getting-started/user-timeline-dropdown.png
   :width: 800

Alternativt kan du bruge vores genvejsmenu ved at taste “Ctrl + q” på dit keyboard, herefter “2”-tasten og så er du i gang.

.. image:: /images/getting-started/user-hotkey.png
   :width: 800


Når du har udført synsprøve og eventuelle målinger, vælger du "Gem og luk journal" nederst på siden, for at journalføre synsprøven.
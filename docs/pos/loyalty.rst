Loyalitetspoint
===============

Når du handler kan du give dine kunder loyalitetspoint automatisk efter en handel. Så kan de bruge pengene igen senere som rabat.


Flytning af loyalitetspoint
---------------------------

Hvis du gerne vil flytte loyalitetspoint fra en konto til en anden, så kan du fjerne loyalitetspoint fra en konto og give dem til en anden kunde.

Her går vi igennem step by step hvad du skal gøre.


Først går du ind på kunden du ønsker at fjerne point fra, og klikker på salgsfanebladet i toppen.

.. image:: /images/pos_loyalty_overview.png
   :width: 800


Dernæste klikker du på **Indsæt penge**, denne knap bruges både til at indsætte og fjerne penge fra deres saldo.

Hvis du ønsker at fjerne 100 point, så skriv **-100** i beløb, og skriv et notat i feltet **Tekst**, så andre ved hvorfor pengene er flyttet.

.. image:: /images/pos_loyalty_subtract.png
   :width: 800


Nu kan du se en oversigt over pengene er væk fra kundens konto.

.. image:: /images/pos_loyalty_moved.png
   :width: 800


Gå så til kunden som du ønsker at overføre penge til, og klik på den samme knap som før **Indsæt penge**.

.. image:: /images/pos_loyalty_to_customer.png
   :width: 800


Nu vælger du den som du ønsker at overføre penge til, og klik på den samme knap som før **Indsæt penge**.

Vælg her det beløb du lige har fjernet fra den anden kunde **100**, og skriv et notat som før om hvorfor pengene er overført til kunden.

.. image:: /images/pos_loyalty_to_deposit.png
   :width: 800


Du kan nu se en oversigt over at pengene er flyttet, og kunden kan nu bruge disse penge i et salg.

.. image:: /images/pos_loyalty_to_overview.png
   :width: 800


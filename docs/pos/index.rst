Kasseapparat
============

Her er en indholdsfortegnelse til forskellige handlinger i kasseapperatet.

.. toctree::
    :maxdepth: 1

    /docs/pos/loyalty.rst
    /docs/pos/sharing.rst


Oversigt
--------

Kasseapparatet i OptikPartner viser som standard de 16 produkter, du sælger mest, for at gøre det hurtigt for dig at finde de produkter, du søger.

Du har mulighed for at tilpasse kasseapparatet og dets knapper, præcis som du ønsker.

Du kan lave 4 kategorier, som kunne være ex. **Progressive**, **Enkeltstyrke**, **Andet**, eller hvad der passer ind i din butik.

Så får du 4 menuer, hvor du har adgang til i alt 64 produkter med 1 klik med musen.

Det ser ud som på billedet herunder.

.. image:: /images/pos-overview.png
   :width: 800


For at indstille disse menuer skal du blot gå til **Indstillinger > System > Hurtigtaster**. Her kan du indstille kasseapparatet som du ønsker.


Hurtigtaster
~~~~~~~~~~~~

Du kan lave lige så mange menuer som du ønsker, og skifte mellem dem som du ønsker, eksempelvis hvis du har specielle kampagner hver torsdag, kan du skifte hver torsdag til en speciel menu.

Ved at vælge **nuværende** ud for en menu, vil det være den aktuelle som bliver vist som standard.

Hver menu kan yderligere blive koblet på en kategori, læs herom nedenfor.

Du kan også lave produkt bundler, og tilfølge knapper som tilføjer flere produkter ad gangen hvis du ønsker.

.. image:: /images/pos-quickkeys.png
   :width: 800


Kategorier
----------

Opret kategorier som du ønsker, du kan sagtens vedhæfte et billede hvis du ønsker. Gå først til **Indstillinger > System > Hurtigtaster**, brug menuen **Kategori** øverst.

Hver menu har et felt hvor du kan vælge en menu, det vil være denne aktuelle menu som bliver vist når du klikker på kategorien i kasseapparatet.

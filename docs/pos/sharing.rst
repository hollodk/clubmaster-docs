Deling af betaling
==================

I nogle tilfælde vil der være behov for at benytte sig af egenbetaling, og få det nemt udspecificeret på regningen.


Her har du en gennemgang
------------------------

Først, opret et salg helt som du plejer, og kobl en kunde samt en anden betaler på.

Vælg nu knappen <b>Delinger</b> i højre kolunne.

.. image:: /images/pos_share_cart.png
   :width: 800


Indtast nu beløbet som enten kunden eller anden betaler skal betale, det andet beløb vil automatisk få tillagt differencen.

.. image:: /images/pos_share_share.png
   :width: 800


Gå så til betalingssiden, her kan du se under både kunden samt den anden betaler hvor meget de hver især skal betale.

.. image:: /images/pos_share_pay.png
   :width: 800


Når betalingen er gennemført, kan du nemt og overskueligt se hvad andel kunden, og hvor meget den anden betaler har betalt.

Ligeledes kommer det også på regningen som bliver sendt ud.

.. image:: /images/pos_share_overview.png
   :width: 800



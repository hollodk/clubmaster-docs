Påmindelser
===========

Påmindelser og standard beskeder kan være gode for kundetilfredsheden af flere årsager.

For det første kan påmindelser hjælpe med at forhindre kunder i at glemme aftaler eller deadlines, hvilket kan reducere frustration og stress hos kunderne og samtidig hjælpe med at opretholde en effektiv og produktiv forretningsproces.

For det andet kan standard beskeder være med til at sikre en ensartet kommunikation med kunderne, hvilket kan bidrage til at opbygge et stærkt og troværdigt brand.

Endelig kan automatiserede påmindelser og standard beskeder også være mere omkostningseffektive for virksomhederne, da det kan reducere behovet for manuel opfølgning og dermed frigøre tid og ressourcer til andre opgaver.

Alt i alt kan påmindelser og standard beskeder bidrage til at øge kundetilfredsheden ved at reducere frustration og stress, opbygge et stærkt brand og være mere omkostningseffektive for virksomhederne.


Opret skabeloner
----------------

Før du går igang med at oprette påmindelser, vil vi anbefale at få oprettet nogen gode skabeloner som gør det nemt og hurtigt at lave påmindelser på kunderne.

Gå først **Indstillinger > Marketing > Skabeloner**.

Her kan du i menuen vælge **Importer skabelon**, her har vi en stak standard tekster du frit kan benytte.

*Husk altid at gennemlæse skabelonen nøje, da der kan være webadresser eller andre ting som skal tilpasses dig specifikt.*


.. image:: /images/template-import.png
   :width: 800


Typer af skabeloner
-------------------

Vi har forskellige typer af skabeloner du kan benytte til dine kunder:

**Påmindelse**  
Den bliver brugt når du opretter påmindelser

**Message template**  
Når du vil sende en besked til en kunde, kommer denne frem som en mulighed

**Kontrakt**  
Her kan du have nogen færdige kontrakter du kan sende til kunden til digital underskrift

**Email invoice template**  
Hvis du vil overskrive vores standard email skabelon


Opret påmindelse på kunde
-------------------------

Når du vil oprette en påmindelse på en kunde, skal du gå til kundens profil side.

Herfra klik i højre side på **Journal / Noter** knappen.

Vælg her **Opret påmindelse**

Når du vælger din påmindelse, vær særlig opmærksom på om det skal være en **Gentagende påmindelse**, så skal du også udfylde feltet **Repeat interval**, og vælge om den skal sendes hver 1 måned, halve eller hele år.


.. image:: /images/reminder-create.png
   :width: 800

.. image:: /images/reminder-modal.png
   :width: 800


Når du har gjort dit valg, så bliver du præsenteret for dit valg, skal der laves specifikke ændringer kan du hurtigt gøre dette.


Flet kunder
===========

Fletning af kunder
------------------

Det kan ske at der er dubletter af dine kunder, det kan ske ved importering af kunder fra andet system, online booking eller blot ved manuelle fejl.

For at løse dette, har vi her en hurtig gennemgang af hvad du skal gøre.

Lad os antage du har 2 kunder:

 - **Kunde 1**, rigtig kunde
 - **Kunde 2**, dublet ønsker at flytte


1. Find først kunde nummeret på **Kunde 1**
2. Find derefter **Kunde 2**
3. Når du står på **Kunde 2**, vælg så den blå pil over kundensnavn til højre og vælg her **Rediger**
4. Nederst på siden vælg her **Flet til anden bruger**
5. Vælg i søgefeltet kundenummeret på **Kunde 1**
6. Når du har fundet kunden, marker de kundedata du ønsker at flytte over
7. Vælg så **Flet kunder**

.. image:: /images/user-merge-1.png
   :width: 800

.. image:: /images/user-merge-2.png
   :width: 800


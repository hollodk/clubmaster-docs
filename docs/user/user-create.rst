Opret bruger
============

Brug af kortlæser
-----------------

Hvis du har tilkoblet en kortlæser til OptikPartner er der 2 måder du kan bruge det for at søge og/eller oprette nye kunder.


Søgning via hotkeys
~~~~~~~~~~~~~~~~~~~

Åben hurtigsøgningsfunktionen, enten ved at klikke på forstørrelsesglas øverst i højre side af dit vindue, alternativt ved **F2** eller **Ctrl+q** på mac.

Kørt nu kortet igennem, og hvis der findes en kunde med dette CPR nummer, vil du se kunden i resultat listen.

.. image:: /images/user-hotkeys-search.png
   :width: 800


Oprette bruger
~~~~~~~~~~~~~~

Hvis du ikke finder kunden i ovenstående tilfælde, så kan du blot klikke på knappen under søgefeltet **Opret kunde**, og du vil nu være ved at oprette en ny kunde med alle informationerne fra sygesikringskortet.

.. image:: /images/user-create-create.png
   :width: 800


Opret via aftaler
~~~~~~~~~~~~~~~~~

Når du er etter en ny aftale i kalenderen, gå da til **Tilføj ny kunde**, og kør herefter sygesikringskortet igennem.

Alle data vil herefter automatisk blive udfyldt.

.. image:: /images/user-create-appointment.png
   :width: 800

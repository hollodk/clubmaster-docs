Grupper
=======

Opret nye grupper
-----------------

Du kan oprette 2 typer af grupper i OptikPartner. Statiske grupper du selv tildeler medlemmer, og dynamiske grupper hvor de bliver langt ind med filtresøgninger som automatisk tilpasser sig.

Grupper tilgår du via **Indstillinger > Sortinger > Grupper**.


Statiske grupper
~~~~~~~~~~~~~~~~

Den lette metode er statiske grupper, her laver du blot en ny gruppe via **Indstillinger > Sortering > Grupper**.

.. image:: /images/group-new.png
   :width: 800


Her tildeler du evt. **roller** hvis medlemmer af denne gruppe skal have specielle rettigheder.

Ellers når du klikker **Gem** er du færdig med at oprette gruppen.

Her tildeler du nye medlemmer ved at redigere din kunde, og her kan du se et felt kalder under fanen **Udvidet** kaldet **Grupper** hvor du kan tildele alle de grupper du ønsker.

.. image:: /images/user-assign-group.png
   :width: 800


Dynamiske grupper
~~~~~~~~~~~~~~~~~

Dynamiske grupper kan du lave ved at tildele en gruppe filtre. Her kan du vælge medlemmer ud fra:

- alder
- senest oprettet
- senest køb
- konto saldo
- og meget andet

Du opretter gruppen som en statisk gruppe, og når du er inde på en gruppe, kan du se i højre side en knap som hedder **Filtre**, klik på den for at indstille gruppens filtre.

**HUSK AT HVIS DU LAVER EN STATISK GRUPPE TIL DYNAMISK, MISTER DEN SINE GRUPPE MEDLEMMER.**

.. image:: /images/group-show.png
   :width: 800


Indstilling af filtre
~~~~~~~~~~~~~~~~~~~~~

Når du opretter gruppe filtre, kan du eksempelvis lave et filter med:

  | **Alle unge under 30**
  |
  | Første kolunne **Age*
  | Næste kolunne **Less than**
  | Sidste kolunne **30**

Dernæst, balance opsætning

  | **Som har en balance under 0**
  |
  | Første kolunne **Balance**
  | Næste kolunne **Less than**
  | Sidste kolunne **0**

På den måde har du lavet et filtre med alle yngre som skylder dig penge.

.. image:: /images/group-filters.png
   :width: 800


Gå nu tilbage til gruppens side for at se hvor mange medlemmer der er i gruppen efter du har tilføjet dine filtre, for at verificere du ikke har lavet en fejl i filteret.

Tag endelig kontakt til os for at hjælpe med at sætte filtre op :)


Download gruppe
---------------

Har du behov for at downloade alle medlemmer fra en gruppe, for at bruge dem i anden sammenhæng kan du sagtens gøre dette også.

Gå blot til gruppen via **Indstillinger > Sortering > Grupper**.

Vælg gruppen du ønsker at arbejde med, og klik i toppen **Download**.

.. image:: /images/group-download.png
   :width: 800



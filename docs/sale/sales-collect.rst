Rykkerflow
==========

Når du kører med salgsopkrævninger eller abonnement, så har OptikPartner mulighed for at sende automatiske rykkere til kunderne.

Opret opkrævning
----------------

Gennemfør et salg normalt, og læg det på kundens konto.

Derefter går du ind i salgsdetaljer og vælger knappen **Opkræv**.

Der bliver her ikke sendt nogen besked til kunden vedrørende opkrævningen, kun en normal faktura hvis du har valgt dette.

.. image:: /images/sales-create-collect.png
   :width: 800


Opkrævning
----------

Hvis kunden har en betalingsaftale vil OptikPartner automatisk trække beløbet fra kundens aftale.

Hvis der ikke er nogen betalingsaftale, eller kortet er udløbet, så sender OptikPartner en reminder email om der mangler betaling.

Heri er der et link, hvor kunden kan enten betale som en engangsbetaling, eller de kan oprette deres kort som en aftale, og vi kan fremtidig trække betalinger automatisk.

.. image:: /images/sale-checkout-collect.png
   :width: 800


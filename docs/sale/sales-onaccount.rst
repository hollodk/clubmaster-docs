Salg på konto
=============

Hvis din kunde køber noget før de betaler, kan du registrer beløbet på kundens konto, så får du et godt overblik over skyldnere.

Lav faktura
-----------

- Opret et nyt salg.
- Læg produkter i kurven.
- Vælg korrekt kunde
- Vælg nu **Betal** knappen under kurven
- Og vælg betalingsform **På Konto**.

Du kan nu se hvis du går ind på kundens profil at vedkommende skylder det beløb du har lagt på kundens konto.

Yderligere vil kunden også fremgå af dit **Overblik**, øverst i toppen kan du se boksen **Tilgodehavende**, klik her på mere info
og du vil nu se kunden fremgår med sin gæld.


Indbetaling
-----------

Når kunden så har overført sin betaling, om det er via MobilePay, bankoverførsel eller på anden vis.

Så går du blot ind på kunden, og klikker på knappen under kundens profil informationer som hedder **Betal udestående/Indbetal**.


Manual
~~~~~~

Vælg så fanen **Manuel**, og du ser følgende skærmbillede.

.. image:: /images/sale-payment-manual.png
   :width: 800

Husk at gennemgå alle felterne, eksempelvis:

**Beløb**, er hvor meget kunden netop har betalt. [KRÆVET]

**Dato**, er den bogførte dato. [KRÆVET]

**Betalingsmetode**, skal stemme med hvilken betalingsform, så dit regnskab stemmer. [KRÆVET]

**Kommentar**, er som en note til dig selv, du måske skal huske senere.

**Pengekasse**, er hvis det skal indgå under dagens kasseopgørelse.

**Udlign salg**, er hvis du vil udligne med et specifikt salg.

Når du er færdig, klikker du blot på **Registrer betaling**, og kundens konto er nu udlignet, og alt stemmer.


Kasseapperat
~~~~~~~~~~~~

Hvis du skal bruge en integration som eksempelvis din betalingsterminal skal du gå igennem kasseapperatet, vælg da fanen **Pengekasse**.

Du kan i bunden se hvor meget gæld kunden har på sin konto.

Så indtaster du blot hvor meget kunden skal indbetale i **Beløb at betale** feltet, og vælger betalingsform.

.. image:: /images/sale-payment-pos.png
   :width: 800


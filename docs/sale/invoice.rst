Faktura
=======

Forskellige indstillinger til faktura opsætning i OptikPartner.


Skifte faktura layout
---------------------

I OptikPartner har vi mange forskellige faktura layouts.

Du kan ind i **Indstillinger > Økonomi > Faktura** rette hvordan en faktura skal se ud.

Rediger en eksisterende faktura, og find menuen dropdown menuen som hedder **layout**, her kan du vælge mellem nogen forskellige modeller,
du kan nemt skifte dem ud og finde en som passer dig.

Går du tilbage til listen over alle dine fakturaer, kan du se en knap der hedder **print**, her kan du se et eksempel på dens opsætning.

.. image:: /images/invoice.png
   :width: 800


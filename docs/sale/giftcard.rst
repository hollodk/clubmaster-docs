Salg gavekort
=============

Du har mulighed for både at sælge og bruge gavekort i forhold til køb.


Salg af gavekort
----------------

Når du skal sælge et gavekort, går du ind i **Ekspeditionen**, i højre menu finder du knappen **Find gavekort**.


Nyt gavekort
~~~~~~~~~~~~

Hvis du skal sælge et nyt gavekort, så vælger du blot her knappen **Find eller opret gavekort**.

Så opretter vi automatisk et nyt gavekort med det næste nummer i rækken.

Vælg så det beløb du ønsker at få lagt på gavekortet, og noter nummeret hvis du skal give kunden et speciel gavekort, ellers kommer det også på kvitteringen.

.. image:: /images/sale-create-giftcard.png
   :width: 800


Optank eksisterende gavekort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ved optankning er det fuldstændig det samme som ovenfor, i menuen **Find gavekort**, indtaster du blot det eksisterende nummer på gavekortet kunden er kommet ind med, og beløbet du indtaster bliver automatisk tanket op på kortet.


Gennemført salg
~~~~~~~~~~~~~~~

Gennemfør nu salget, og gavekortet er blevet oprettet og klar til brug.


Køb med gavekort
----------------

Når en kunde kommer ind og skal betale med sit gavekort, så gennemfører du blot et normalt salg, lægger varerne i kurven som normalt.

Vælger **Betal** knappen, så du kan se betalingsformer.

Du har nu en betalingsform som hedder **Gavekort**, klik på denne, og en menu kommer op hvor du indtaster kundens gavekort nummer.

Resten sker som en helt normal betalingsprocess, er der penge tilbage på købet kan kunden lave en restbetaling. Ellers bliver salget nu gennemført.

.. image:: /images/sale-use-giftcard.png
   :width: 800


Kontrakter og betaling
======================

Her har vi en oversigt over hvad dine kunder kan gøre selv, for at gøre dit arbejde meget lettere.

Først har vi kontrakter du kan sende ud til dine kunder, derefter har vi online betaling hvordan det fungere.

Skabeloner som du skal være interesseret i at kigge igennem er:

- Betalingsbetingelser (se **indstillinger > skabelon** terms and conditions)
- Abonnementsbetingelser (se **indstillinger > skabelon** subscription terms)
- Kontrakter koblet på abonnementet (se **indstillinger > skabeloner** contract, og du kan tilføje disse til abonnementstyper)


Opret abonnement
^^^^^^^^^^^^^^^^

Oprettelse af abonnement skal du altid gøre først, der kan du følge vores guide som hjælper med dette.

.. image:: /images/subscription-overview.png
   :width: 800


Online kontrakter
^^^^^^^^^^^^^^^^^

Når du har lavet dit abonnement til en kunde, skal du klikke på fanen **Kontrakt**.

.. image:: /images/subscription-contract-overview.png
   :width: 800


Du kan så enten sende en kontrakt til kunden via email. Så kan kunden underskrive og læse kontrakten igennem i ro og mag hjemme.

1. Klik på knappen **Online version** for at se hjælpe kunden igennem kontrakten og evt. underskrive i butikken

2. Klik på knappen **Send email** og skriv kundens email adresse for at sende kontrakten så de kan gennemgå det hele hjemmefra.

.. image:: /images/subscription-contract-email.png
   :width: 800


Nu kan du via linket **Online version**, se det samme som kunden ser hjemme hos dem selv, her har kunden følgende muligheder:

- Gennemlæse og acceptere betalingsbetingelser
- Gennemlæse og acceptere abonnementsbetingelser
- Digital underskrift via tablet, smartphone eller mus til computer
- Tilmelde betalingskort, mere om det herefter

.. image:: /images/subscription-online.png
   :width: 800


Online betaling
^^^^^^^^^^^^^^^

For at lave online betalinger i OptikPartner skal du bruge en PensoPay eller PBS aftale.

Hvis du har aktiveret onlinebetalinger i OptikPartner kan kunden nu også bruge sine betalingskort for at betale sit abonnement.

Det letteste er PensoPay med kreditkort betaling, det fungere helt på samme måde som dit fitness medlemskab.

Flowet er på samme måde som med **Online kontrakter** herover.

Når kunden har accepteret abonnementet online, kan kunden tilmelde sit kreditkort, dette gøres simpelt ved at vælge **Fortsæt til betaling**.

.. image:: /images/subscription-online.png
   :width: 800


Så snart betalingen er gennemført er kundens abonnement aktivt, og fremtidige trækninger bliver nu automatisk trukket.


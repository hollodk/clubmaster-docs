Opsigelse af abonnement
=======================

Når en kunde ønsker at opsige sit abonnement, er der to ting, du skal have in mente.

Du skal først tjekke, om der er nogen manglende betalinger, som kunden endnu ikke har betalt, eller om der skal krediteres eventuelle ekstra betalinger.


Opsigelse af abonnement
^^^^^^^^^^^^^^^^^^^^^^^

Første led er under alle omstændigheder at afslutte abonnementets fornyelse.

Gå derfor til kundens abonnement, så du har fanerne vedr. abonnementet i toppen af din skærm, vælg her **Afslut abonnement**.


.. image:: /images/subscription-cancel-overview.png
   :width: 800


Opsigelses dato
^^^^^^^^^^^^^^^

Valg af opsigelsesdato.

Når du klikker på knappen **Afslut fornyelse**, kan du vælg om abonnementet skal afsluttes dags dato, eller i fremtiden.

Hvis kunden har fremtidige betalinger liggende, skal du også tage stilling til om disse skal slettes eller om kunden fortsat skal betale dem.

.. image:: /images/subscription-cancel-cancel.png
   :width: 800


Afsluttet og færdiggør
^^^^^^^^^^^^^^^^^^^^^^

Når abonnementet er afsluttet skal du sikre dig at kunden er afsluttet korrekt, det vil sige:

- Åbne salg skal enten slettes eller bliver de trukket.
- Processed salg har overskrevet trækningsdato og skal derfor krediteres hvis ikke kunden skal betale.

.. image:: /images/subscription-cancel-ended.png
   :width: 800


Slette salg
^^^^^^^^^^^

Hvis du da du afsluttet valgte **behold åbne salg**, men har fortrudt, kan du stadig slette dem.

Vælg blot **Pilen > Vis** ud for salget, og vælg herefter den røde knap **Slet**. Dette kan du kun gøre ved et åbent salg, ikke når det er i status **Processed**.

.. image:: /images/subscription-cancel-delete-sale.png
   :width: 800


Krediter salg
^^^^^^^^^^^^^

Hvis et salg derimod er overskredet trækningsdato skal salget krediteres på grund af dit bogholderi.

Vælg blot **Pilen > Vis** ud for salget, og her kan du gennemføre en helt normal kreditering, salget vil være gennemført **På konto**, så det skal blot krediteres tilbage **På konto**.

Læs mere om krediteringer her:

:doc:`/docs/getting-started/pos-refund`


.. image:: /images/subscription-cancel-refund.png
   :width: 800


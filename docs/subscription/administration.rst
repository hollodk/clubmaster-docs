Abonnementersadministration
===========================

Når du har abonnementer i din butik, så er det vigste at sikre at du får pengene ind, og at få et indblik i hvordan din butik vægster.

Dette afsnit af fælles for både linseabonnementer og brilleleje abonnementer.


Se forældet betalinger
~~~~~~~~~~~~~~~~~~~~~~

Går du til menupuntet **Indstillinger > Abonnement > Opkræv** kan du nemt se en liste over alle de kunder som mangler at betale en eller flere regninger.


Se fejlbetalinger
~~~~~~~~~~~~~~~~~

For at gøre det lettere at drive din forretning, så har vi nogen rigtig gode værktøjer der hjælper dig med at finde dårligere betalere.

Hvis du vælger OptikPartners betalingsløsning, så sender vi løbende opkrævninger ud til kunderne som ikke har betalt, men engang i mellem er der nogen kunder som tror de kan forsvinde i mængden, så er det vigtigt at du har de rette værktøjer til at finde disse kunder.


Først, så kan du altid gå til **Indstillinger > Abonnement > Betalinger**, her kan du sortere på fejlbetalinger, her kan du se fra dag til dag hvis er er nogen problemer med kundernes betalingsform, både PBS og kreditkort.


Derudover kan du også gå til **Indstillinger > Abonnement > Opkræv**, her har du en sorteret liste over hvilke regninger som endnu ikke er betalt fra dine kunder.

Kommer nogen af kunderne or langt bagud, så vil vi anbefale at hive fat i kunderne, før at det går helt galt.


Derudover, synes vi du skal koble **Månelig statistikker** til, og så hver måned gå igennem listen vi sender til dig, der kan du nemt følge med i hvordan din forretning kører.


Rapporteringer
~~~~~~~~~~~~~~

Gå til **Rapporter > Abonnement**, får du et godt overblik over om din abonnementsforreting stiger eller falder.

Går du til **Indstillinger > Abonnement > Overblik**, her kan du få en overblik over hvor meget vi trækker i abonnementfornyelser i næste måned.


Månedlig statistikker
~~~~~~~~~~~~~~~~~~~~~

Du kan via menupunktet **Indstillinger > Statistikker** sætte op at generer et overblik over:

- abonnementsbestillinger næste måned
- abonnementstrækninger i næste måned
- overblik over hvem der skylder penge

Du kan sætte statistikkerne op til at automatisk sende dig en email reminder hver måned.


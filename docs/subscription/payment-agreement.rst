Opsætning af betalingsaftale
============================

Hvis en kunde skal have en ny betalingsaftale, men ikke behøver at underskrive en digital kontrakt, kan du bruge denne guide.


Oprette aftale på en kunde
--------------------------

Find den kunde, du ønsker at oprette en betalingsaftale på.

Gå derefter til salgsfanen og gå lidt ned på siden indtil du kommer til **Betalingsaftaler**, vælg her knappen **Opret betalingsaftale**.

.. image:: /images/payment-agreement-user.png
   :width: 800


I det nye vindue skal du blot vælge **Kreditkort** i betalingstypen. I feltet **Velkomst mail** kan du vælge, om kunden skal modtage en e-mail med et link til at acceptere betalingsaftalen, så de selv kan tilmelde sig på deres egen computer, når de kommer hjem.

.. image:: /images/payment-agreement-create.png
   :width: 800


Accepter aftalen
^^^^^^^^^^^^^^^^

Når du har oprettet en ny aftale, kommer du tilbage til listen over kundens betalingsaftaler. Den aftale, du netop er i gang med at oprette og færdiggøre, ligger øverst på listen.

Hvis kunden stadig er i butikken og hellere vil acceptere og oprette aftalen med det samme i stedet for at gøre det derhjemme via det eventuelt sendte link, så skal du klikke på knappen **Oprettelses-link**.

.. image:: /images/payment-agreement-list.png
   :width: 800

Så vil du få vist en side, hvor kunden skal acceptere butikkens abonnements- og handelsbetingelser og derefter trykke **Gå til betaling**.
Herfter dukker et nyt vindue op, hvor kunden skal udfylde sine kortoplysninger inden der kan trykkes **Create subscription** og betalingsaftalen er gennemført.

.. image:: /images/payments-payments4.jpg
   :width: 800


Opsætning af ny aftale
----------------------

Pensopay
^^^^^^^^

For at kunne benytte dig af abonnementsløsninger i OptikPartner skal du først have en aftale med enten Stripe eller Quickpay/Nets.

Her gennemgår vi en aftale med Quickpay/Nets, men fremgangsmåden for stripe er stort set det samme.

#. Først skal du finde din API key hos PensoPay
#. Derefter opretter du en betalingsaftale i OptikPartner

Find API nøgle hos QuickPay
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Først logger du ind i dit QuickPay interface på https://manage.quickpay.net/.

Gå i hovedmenuen i venstre side og find menuen **Indstillinger**, gå derefter til **Integration**, og du kommer nu til nedenstående billede, hvor du blot klikker på **API key** ud for brugeren **API user**.

Kopier denne værdi med over i OptikPartner og gå til næste punkt.

.. image:: /images/payments-pensopayapi.jpg
   :width: 800


Opsætning af Betalingsintegration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nu har du din nøgle fra QuickPay, gå så i **Settings > External > Betalingsintegration** i OptikPartner.

Her skal du ved udfylde **Secret** med nøglen fra PensoPay/QuickPay.

Du indtaster yderligere hvilke lokationer denne aftale skal vælge gyldig i under **Lokation**, i tilfælde af den skal gælde i alle lokationer, undlad blot at vælge denne.

Du vælger i **Betalingsmetoder** hvilken betalingsform indbetalinger skal behandles som, det vil typisk være relevant i forhold til integration med regnskab.


.. image:: /images/payments-pensopayintegration.jpg
   :width: 800


Dagligdagsopgaver
=================

Der er altid nogle daglige rutiner vedr. et abonnement, læs om de mest gængse opgaver herunder.


Ændre betalingsperiode
^^^^^^^^^^^^^^^^^^^^^^

Hvis kunden ønsker en anden betalingsperiode, så skal du først ændre kommende trækningsperiode, og så kan du tilpasse de kommende trækninger hvis du ønsker dette.


Gå først til kundens abonnement via deres profil side og fanen **Abonnement**.

Klik her **Vis** ud for det abonnement du ønsker at ændre.

.. image:: /images/subscription-period-change-overview.png
   :width: 800


Når du er på abonnementsiden, gå da til fanen **Rediger**.

.. image:: /images/subscription-period-change-edit.png
   :width: 800


Rediger nu til den ønskede tid, som du ønsker næste trækning, her vælger du blot en dato i **Næste betalingsperiode**.

Husk at klikke **Gem** når du er færdig.

Du er nu tilbage på abonnementsoverblikket, her kan du passende bekræfte øverst at **Næste betalingsperiode** er skiftet til den ønskede dato.

.. image:: /images/subscription-period-change-main.png
   :width: 800


**Skift kommende trækninger**

Hvis du også ønsker at ændre de kommende trækninger, som allerede er sat til opkrævning, så kan du følge disse steps for hver trækning nedenfor.

Først vælger du den blå pil ud for den trækning du ønsker at ændre dato på.

.. image:: /images/subscription-period-change-collect-arrow.png
   :width: 800


Vælg herefter **Vis** i menuen.

.. image:: /images/subscription-period-change-collect-sale.png
   :width: 800


Vælg så den blå knap **Opkræv**.

Nu kan du sætte den ønskede dato, du vælger blot den ønskede dato i dato feltet.

.. image:: /images/subscription-period-change-collect-confirm.png
   :width: 800


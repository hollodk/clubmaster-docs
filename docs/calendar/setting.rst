Kalender opsætning
==================

Nedenfor ser du nogen forskellige eksempler som passer til forskellige butikker.


Opsætninger
^^^^^^^^^^^

**Single facility**

Hvis du blot ønsker at se en enkelt kalender, det kan være hvis du har en butik med kun en kalender som bruges til booking, ex. et synsprøve rum, så vil dette være at anbefale.

Denne kalender giver også det bedste overblik hvis du skal se eksempelvis ugeoverblik.

.. image:: /images/calendar-single.png
   :width: 800


**Location facilities**

Denne opsætning viser alle rum som kan bookes i en given lokation, så har du synsprøverum 1 og 2, så vil det være dem du ser side by side.

.. image:: /images/calendar-facilities.png
   :width: 800


**Location facilities + Employees**

Denne opsætning er som ovenfor, blot så viser vi også medarbejdere her med deres fulde kalendre.

.. image:: /images/calendar-facilities-employee.png
   :width: 800


**Location facilities + Employees (allday)**

Denne opsætning er som ovenfor, blot i stedet for at medarbejderne får deres egne kalendre, så bliver der blot udfyldt i toppen af kalenderen i feltet "all-day" hvem der arbejder en given dag.

.. image:: /images/calendar-facilities-employee-allday.png
   :width: 800


**Organization facilities**

Hvis du har flere butikker, og ønsker et fuld overblik over synsprøverum i alle butikkerne på en gang, så kan du gøre det her, den vælger på tværs af alle dine butikker.


**Organization facilities + Employees**

Denne vælger det samme som ovenfor, blot vælger den også at vise medarbejderne med deres fulde kalendre.


**Organization facilities + Employees (allday)**

Denne vælger det samme som ovenfor, blot kommer medarbejderne i "all-day" feltet i stedet for hver sin kalender.


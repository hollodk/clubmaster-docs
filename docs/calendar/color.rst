Kalender farver
===============

OptikPartner kan skrædersyes til alles behov, om du ønsker at se din kalender med udgangspunkt i dine medarbejdere eller produkter, så kan du få dit eget personlige udseende.


Opsætninger
^^^^^^^^^^^

**Medarbejder farver**

Du kan enten sætte din kalender op til at vise medarbejder farver, hvis du går til **Indstillinger > System > Medarbejdere**, så kan du se en liste over hvilke farve de forskellige medarbejdere har.

Dem kan du selv skifte ved at redigere hver medarbejder under fanen **Medarbejder**.

.. image:: /images/calendar-color-employee.png
   :width: 800


Det betyder at ved en kalender aftale, så vil deres tider nu fremstå med netop den valgte farve.

.. image:: /images/calendar-view-color-user.png
   :width: 800


**Produkt farver**

Ønsker du i stedet for at aftalerne vises farvebestemt af hvilken type aftale det er, det kan være om det er en synsprøve eller linsekontrol, så kan du skifte disse farver under dine produkter.

Gå til produktoversigten, for nemt at finde produkter der bruges i kalenderen, skriv da i feltet **Skriv navn eller SKU** følgende:

  is:booking

Og du vil nu se alle produkter som kan bookes i kalenderen.

.. image:: /images/calendar-color-product.png
   :width: 800

.. image:: /images/calendar-color-product-edit.png
   :width: 800


Det gør at aftalerne vil nu skifte farve til at følge farverne for produkterne.

.. image:: /images/calendar-view-color-product.png
   :width: 800


Der er ikke nødvendigvis en rigtig eller forkert måde at gøre det på, det er helt op til hvad der passer din forretning bedst.

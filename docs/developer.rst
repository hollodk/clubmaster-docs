OptikPartner Develper
=====================

Kom igang med OptikPartner udvikler, og hjælp med at forbedre vores dokumentation.


Skriv dokumentation
-------------------

Vores dokumentation ligger offenligt på https://gitlab.com/hollodk/clubmaster-docs.

Vi benytter os af formatet reStructuredText, som er meget let at lære og forstå.

Benyt evt. forskellige online editors for at komme igang:

- http://rst.ninjs.org/


Arbejd og gem filer
-------------------

En nem måde at, komme igang med er med GitKraken til windows.

Følg vores beskrivelse her af hvordan du benytter dig af :doc:`git`.


Tilføj en ny side
-----------------

Når du tilføjer en ny side til dokumentationen, er der få ting du skal huske.

* alle filer kan kun indeholde følgende tegn, a-z, 0-9 samt bindestreg, altså ingen danske tegn eller mellemrum. ex. user-create.png, eller user-create.rst
* billeder skal lægges i images/ mappen
* dokumenter lægges i docs/ mappen
* billeder skal uploades som enten jpg eller png filer
* dokumenter skal uploades som rst filer
* husk at inddele dokumenter i logiske lokationer ex. docs/integration/verifone.rst
* når du har tilføjet et nyt dokument, skal du huske at det skal med i et index, enten på forsiden eller via en underside


Lær formatet
------------

Kom godt igang med reStructuredText formatet.

https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html


For nogen lidt mere avanceret funktioner

https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html


Cheatsheet

https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html


Opsætning af tema

https://sphinx-themes.org/sample-sites/sphinx-rtd-theme/kitchen-sink/admonitions/


Eksempler
---------

her kan du se en **fed** tekst, og her kan du se en *italic* tekst

hvis du vil ``quotes`` så det sådan her, ellers kan du smide sjove "omkring"


Lister
^^^^^^

hvis du vil lave nogen lister kan du lave dem sådan her nummeret og unummeret

* første emne
* andet emne
    * sub til andet emne
        * og selvfølgelig har vi flere
        * emner her
    * husk os mælk


#. første emne
#. andet emne
    #. og på samme måde
        #. og vi har selvfølgelig
        #. også tredje led her
    #. med underkategorier
    #. fyld tanken :)


Overskrifter
^^^^^^^^^^^^

Forskellige overskriver kan ses her:

Dette er for en part
####################

Dette er for et chapter
***********************

Dette er en sektion
===================

Dette er en subsektion
----------------------

Dette er en subsubsektion
^^^^^^^^^^^^^^^^^^^^^^^^^

Dette er en paragraf
""""""""""""""""""""


Tekst block
^^^^^^^^^^^

dette er bare en test af noget tekst som er over en kasse

    | og her har vi altså indholdet af vores kasse, som er sat flot op :)

    | rigtig god dag huggo!

    | mvh,
    | Michael Holm
    | OptikPartner


Tabeller
^^^^^^^^

Skal noget sættes op i flotte kasser, kan et gøres sådan her

:navn: Michael Holm
:adresse: Röhrliberg 30
:postcode: 6300
:city: Cham


Alternativ table format, lidt mere avanceret og flere kolunner

.. list-table:: Frozen Delights!
   :widths: 15 10 30
   :header-rows: 1

   * - Treat
     - Quantity
     - Description
   * - Albatross
     - 2.99
     - On a stick!
   * - Crunchy Frog
     - 1.49
     - If we took the bones out, it wouldn't be
       crunchy, now would it?
   * - Gannet Ripple
     - 1.99
     - On a stick!


Kasser
^^^^^^

.. note::

   dette er en note


.. warning::

   dette er en advarsel


.. tip::

   du kan sådan set bruge alle følgende til at lave forskellige kasser :)

   * attention
   * caution
   * danger
   * error
   * hint
   * important
   * note
   * tip
   * warning


.. seealso::
   muahah
   det er godt lir ;)


Billeder
^^^^^^^^

og selvfølgelig skal vi også lige have et billede med :)

.. image:: /images/hotkeys-kunde.png
   :width: 800


Lidt mere teknisk
^^^^^^^^^^^^^^^^^

.. versionadded:: 1.3


.. versionchanged:: 1.2


.. deprecated:: 1.1


.. rubric:: OptikPartner 1.3


.. centered:: OptikPartner 1.3


.. hlist::
   :columns: 3

   * A list of
   * short items
   * that should be
   * displayed
   * horizontally
   * and one last


.. rst:directive:: foo

   Foo description.


.. rst:directive:: .. bar:: baz

   Bar description.


.. math:: e^{i\pi} + 1 = 0
   :label: euler


Kom igang med HTML
------------------

Vi har også en lille sektion til at lære at lave HTML kode, kig videre på :doc:`/docs/developer/html`.


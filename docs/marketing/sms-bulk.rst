SMS udsendelse
==============

Oprette sms besked til kunder
-----------------------------

Når du ønsker at sende en fælles besked til dine kunder, skal du gå til **Indstillinger > Marketing > Beskedkampagner**.


Først opret en gruppe
~~~~~~~~~~~~~~~~~~~~~

Du skal først sikre dig, at du har en gruppe, du ønsker at sende til, den kan du vælge i menuen øverst **Grupper**.

.. image:: /images/message-campaign-index.png
   :width: 800


Følg blot vores guide her :doc:`/docs/user/group`.


Opret ny kampagne
~~~~~~~~~~~~~~~~~

Under oversigten kan du vælge knappen **Opret ny**.

Vælg her **Kategori**, hvis du vælger marketing **SKAL** kunden have givet accept til marketing, det er et spørgsmål om at overholde spam regler.

Yderligere kan du sætte en valgfri afsender adresse, du kan sætte din egen adresse hvis du ønsker at få svar på din mobiltelefon.

Du kan selv bestemme, hvornår beskeden skal sendes ud, i feltet **Send at**, det kan være nu, eller om 1 uge.

Du kan også oprette en 2-vejs kommunikation i OptikPartner, tag fat i os og hør nærmere herom.

  | **VIGTIGT**
  |
  | Når du sender beskeder betaler du en pris for hver SMS, det vil sige 160 tegn til 100 personer er 100 sms'er.
  | Hvis din besked er 200 tegn er det 2 beskeder til 100 personer, altså 200 sms'er.
  |
  | Når du skriver din besked, kan du følge med nederst i vinduet, hvor du skriver antal tegn.

Når du er færdig klik **Vælg modtagere**.

.. image:: /images/campaign-message-new.png
   :width: 800


Vælg modtagere
~~~~~~~~~~~~~~

Nu skal du til at vælge modtagere, du kan her vælge **Alle**, eller du kan vælge nogen fra en bestemt gruppe du netop har oprettet.

Du overføre til modtagere ved blot at klikke på **Tilføj**, og kan så se hvor mange der er i gruppen i højre side.

.. image:: /images/campaign-message-recipients.png
   :width: 800


Send besked
~~~~~~~~~~~

Når du er tilfreds, klikker du blot på knappen **Klar til at sende**.

Beskeden vil herefter blive gennemgået af vores team for at sikre der ikke er fejl eller juridiske problemer jf. lovgivningen, og bliver kort herefter sendt til dine kunder.


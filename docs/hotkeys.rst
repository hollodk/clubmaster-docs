HotKeys
=======

I OptikPartner har vi en quickkey funktion, der kan bruges alle steder i platformen .

Tryk F2 eller Ctrl+Q på alle sider, og en hurtig søgeside med adgang til alle relevante funktioner kommer op.


Tips til søgning
----------------

Når du søger i OptikPartner

.. image:: /images/hotkeys-kunde.png
   :width: 1000


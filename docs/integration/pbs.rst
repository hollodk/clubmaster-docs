PBS
===

Små tips og tricks til PBS.


Oprette aftale
--------------

For at lave en ny PBS aftale på kunden kan du enten indtaste dine kunders oplysninger i OptikPartner, eller lade kunden selv gøre det.

Først skal du gå til din kunde, og vælge fanen **Abonnement**.

Gå så ned i **Opret betalingsaftale**.

Vælg her type **Betalingsservice** og payment integration **Pbs**.

.. image:: /images/integration-pbs-create.png
   :width: 800


Du kan her selv udfylde informationerne **CPR**, **Bank reg** og **Bank konto**, eller du kan sende velkomst mail til kunden, og de kan selv gøre det hjemme hos dem selv.


Oprettelses link
~~~~~~~~~~~~~~~~

Hvis du har sendt en velkomst mail til kunden får de en mail, hvor de selv kan indtaste oplysningerne, alternativt kan du finde deres oprettelseslink:

- Gå til kundens profile.
- Gå til abonnement
- Vælg den blå pil ud for PBS betalingsaftalen
- Vælg oprettelseslink

.. image:: /images/integration-pbs-link.png
   :width: 800


Indtast oplysninger
~~~~~~~~~~~~~~~~~~~

Her ser du hvordan siden dine kunder ser når du sender dem et link, her kan de indtaste følgende:

- CPR nummer
- Konto registreringsnummer
- Konto nummer

.. image:: /images/integration-pbs-sign.png
   :width: 800


Tips
----

Når du opretter et nyt mandat for kunden, så vil du dagen efter du har oprettet det kunne se det er aktivt i OptikPartner.

Engang i mellem kan de fejle på, kundens CPR-nummer er forkert, eller bankoplysninger er forkert.

Disse oplysninger vil vi ikke kunne se i OptikPartner, men der kan man følge med på PBS brugerportal i vil have adgang til.

Her vil alle disse fejlbeskeder figurer hvorfor mandatet ikke er oprettet.


Skifte bank oplysninger
-----------------------

Når kunden skifter bank, så er det letteste at kunden ringer til sin nye bank, og beder dem om at flytte alle deres aftaler, og så fungere alting fuldstændig automatisk.


Hvis ikke kunden ønsker at gøre dette, så kan i i OptikPartner oprette en ny betalingsaftale, og lade den gamle ligge, så vil OptikPartner automatisk afmelde den gamle, og dagen efter oprette en ny aftale på kunden.


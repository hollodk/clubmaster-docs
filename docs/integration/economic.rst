Economic
========

Her er en komplet gennemgang fra en helt ny economic konto, hvordan man kobler OptikPartner og economic sammen med alle steps.


Kom igang
---------

Her er rækkefølgen af hvordan vi får sat economic op med OptikPartner.

#. Send følgende link til revisor som kan returnere med en token https://secure.e-conomic.com/secure/api1/requestaccess.aspx?appPublicToken=sn8SP0pABpUaCggKZy44NgnXBqZMgS6YR6FdBlygHYA1
#. Opret ny integration, og udfyld blot **token** og evt. andet required
#. Nu kan du via listen klikke **debug** og se alle oplysningerne i economic vi skal bruge


Skaf token
----------

Følg den normale procedure for at skaffe en token via OptikPartners hjemmeside.

.. image:: /images/economic-token.png
   :width: 800


Find oplysninger
----------------

For at udfylde integrationen skal du benytte “debug” knappen, her slår vi alle informationerne op fra kundens regnskabssystem i economic.
Alle disse numre man ser her i integrationen skal bruges til at udfylde i OptikPartner, så vi ved:

- hvordan opretter vi nye kunder i economic
- hvordan opretter vi produkter i economic
- hvordan skal faktura se ud i economic
- *..* altså alle indstillingerne er for at vi kan oprette ting korrekt og automatisk i economic for kunden


    | **Eksempelvis**
    |
    | *Kassekladde*
    | i nedenstående eksempel kan du se at vi finder 2 kassekladder i economic, **Daglig** og **Indbetalinger**, typisk vil kunden oprette en ny
    | som hedder **OptikPartner**, hvor vi kan lægge alle vores kassekladder.
    | Hvis nu vi skal lægge alle vores indbetalinger ind på kassekladden **Daglig**, så skal vi i **Payment Account** skrive tallet **2**.
    |
    | *Kunde gruppe*
    | i nedenstående eksempel kan man se at i dette tilfælde har kunden i economic en **kunde gruppe** der hedder **Diverse**, for at vi opretter
    | nye kunder i gruppen **Diverse** i economic når der kommer nye kunder, så skal vi i OptikPartner udfylde feltet **User Group Number** med 1.
    | Og alle nye kunder der bliver oprettet, bliver derefter oprettet i denne gruppe.


.. image:: /images/economic-information.png
   :width: 800


Disse informationer skal du lægge ind i følgende felter i opsætning af en ny profil i economic:


.. image:: /images/economic-setup.png
   :width: 800


.. list-table:: Integrationsoversigt
   :widths: 15 10 30
   :header-rows: 1

   * - Felt
     - Type
     - Beskrivelse
   * - User Group Number
     - invoice
     - Hvilket kunde gruppe nye kunder skal ind i i economic
   *
     - user vat zone number
     - invoice
     - Hvilken moms zone nye kunder skal ind i
   *
     - standard payment terms
     - invoice
     - Hvilke betalingsbetingelser en faktura skal oprettes med i economic
   *
     - layout number
     - invoice
     - Hvilken layout nummer en faktura skal oprettes med i economic
   *
     - accounting year
     - invoice
     - Hvad er indeværende regnskabsår i economic
   *
     - payment account
     - invoice, ledger
     - Hvilken kassekladde lægger vi indbetalinger ind på
   *
     - sale account
     - ledger
     - I tilfælde af man kører ren ledgers integration, hvilken kassekladde skal vi lægge salg ind på


Opret produkt
-------------

Opret et produkt i OptikPartner, her skal du blot huske:

- Navn på produkt
- Pris
- Produkt gruppe
- Moms

Når du har disse 4 ting, så er du klar til at sælge produkter via OptikPartner.

.. image:: /images/economic-product-create.png
   :width: 800


Opret betalingsmetode
---------------------

For at du kan modtage betaling, og indbetalingerne kommer over i economic, så skal du blot vælge en konto plan, og så er du færdig med betalingsmetoder.

.. image:: /images/economic-payment-method-create.png
   :width: 800


Gennemfør salg
--------------

Lav nu et salg, og indenfor 1 minut er salget ovre i economic.

.. image:: /images/economic-sale.png
   :width: 800


Opret produkt grupper i Economic
--------------------------------

I tilfælde af produkt gruppen du har valgt tidligre ikke allerede er i Economic, så skal denne oprettes.

**Vær yderst opmærksom på at når du opretter produkter uden moms, skal disse være i en produkt gruppe uden moms, når det gælder Economic.**

.. image:: /images/economic-economic-product-group.png
   :width: 800


Kontroller salg i OptikPartner
------------------------------

Kontroller at salget er overført korrekt til Economic fra OptikPartner via “indstillinger > log > economic”, her skal gerne blot være “logging” samt “ok” tegn, så er alt gået som det skal.

.. image:: /images/economic-sale-view.png
   :width: 800


Vis indbetaling i Economic
--------------------------

På samme måde med indbetalingerne, kontroller at dine indbetalinger er kommet over som forventet.

.. image:: /images/economic-economic-payment.png
   :width: 800


Vis salg i Economic
-------------------

Sikre dig at salget er som det skal være i economic, her finder du fakturaen.

.. image:: /images/economic-economic-invoice.png
   :width: 800


Find faktura fra OptikPartner
-----------------------------

Når du har overført en faktura fra OptikPartner og skal genfinde den i E-conomics, kan den enten være i **Salg > Fakturaer** i e-conomics, eller **Salg > Arkiv** alt efter om den allerede er bogført.

Er den bogført, gå da til **Salg > Arkiv**, her kan du nemt søge den frem, udfyld blot feltet **Tekst** med faktura nummer fra OptikPartner, og klik **OK**.

.. image:: /images/economic-archive-search.png
   :width: 800


Du vil nu se alle resultaterne, hvoraf din faktura vil fremgå som vist på nedenstående billede.

.. image:: /images/economic-archive-result.png
   :width: 800


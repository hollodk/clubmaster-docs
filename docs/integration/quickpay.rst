Quickpay
========

Små tips og tricks til Quickpay.


Vis API nøgle
-------------

For at lave en integration med OptikPartner skal vi bruge API-nøglen fra dig.

Gå til https://manage.quickpay.com og log ind med dine brugeroplysninger.

Gå til menuen **Settings > Integration** og klik på knappen **Api key** med navnet **API user**, du vil nu se en nøgle komme op som illustreret herunder.


.. image:: /images/quickpay-api-key.png
   :width: 800


Tips
----

Lav en manuel betalingsaftale med dine kunder.

For at bruge denne guide skal du have login til https://manage.quickpay.com, alle følgende guides kræver dette.


Opret betalingslink
~~~~~~~~~~~~~~~~~~~

For at lave et manuelt engangsbetalingslink til dine kunder, i din quickpay playform gå til **Tools > Payment link**.

Udfyld felterne, og klik på knappen **Get link**.


.. image:: /images/quickpay-payment-link.png
   :width: 800


Opret aftale
~~~~~~~~~~~~

For at lave en aftale, hvor du kan trække automatisk fra i fremtiden. gå til den samme side, men vælg **Subscription** i stedet for **Payment** i det øverste valg.

Her er beløbet ligemeget da dette blot er for at godkende kundens kortoplysninger.

Husk at gem ordrenummer, da det vil være måden du kan genfinde abonnementet på senere.

Når du ønsker at trække et beløb fra kunden, kan du gå til **Subscription** i hovedmenu, og find det ønsker ordre nummer.

Klik på abonnementet i listen, og vælg **Create recurring**, og vælg så det beløb du ønsker at trække fra kunden.


.. image:: /images/quickpay-subscription-recurring.png
   :width: 800


ZEISS
=====

Integration mellem OptikPartner og ZEISS.


Bestil glas via VisuStore
-------------------------

Når du bestiller glas hos ZEISS, bliver det gjort via deres VisuStore webshop.

Vi overfører recept data til deres system, og så kan du gennemføre ordren i VisuStore, hvorefter vi trækker bestillingen tilbage til OptikPartner, og du kan gennemføre salget som normalt.


Find kunden
~~~~~~~~~~~

Først find kunden i OptikPartner, og gå til ikonet **Overfør** lige under kundens personlige informationer, herefter kan du integrer med andre systemer.

.. image:: /images/zeiss-find-customer.png
   :width: 800


Overfør data
~~~~~~~~~~~~

Vælg nu knappen **Overfør til VisuStore**, og en popup kommer frem, hvor du kan vælge en konklusion.

.. image:: /images/zeiss-transfer-data.png
   :width: 800


Gennemfør glasbestilling
~~~~~~~~~~~~~~~~~~~~~~~~

Gennemfør nu ordren som normalt i ZEISS VisuStore. Vi overføre den til din indkøbskurv, og så kan du arbejde videre på bestillingen, uden du skal til at indtaste nogen oplysninger.

.. image:: /images/zeiss-visustore-cart.png
   :width: 800


Vælg nu et glas som passer til kundens recept.

.. image:: /images/zeiss-visustore-prescription.png
   :width: 800


Når du er færdig lukker du blot vinduet, og vender tilbage til OptikPartner.

.. image:: /images/zeiss-visustore-confirm.png
   :width: 800


Indhent glasbestilling
~~~~~~~~~~~~~~~~~~~~~~

Når du er tilbage i OptikPartner kan du klikke på det lille opdateringstegn øverst til højre i tabellen over bestillinger fra ZEISS, for at hente de seneste nye informationer.

.. image:: /images/zeiss-glass-refresh.png
   :width: 800


Klik derefter på **VIS**, og du får nu mulighed for at vælge **Importer bestilling**, som opretter en glasbestilling.

Den kan du nu tage videre til ekspeditionen som ved normalt glassalg.

.. image:: /images/zeiss-glass-import.png
   :width: 800


Overfør til VisuConsult
-----------------------

Når du overføre data til ZEISS VisuConsult skal du først have installeret vores LiloApp på din computer, for at vi kan forbinde til ZEISS VisuConsult.

Tag fat i din OptikPartner supporter for at få hjælp hertil.

Når den er sat op og kører, så er du klar til at overføre data.


Find kunden
~~~~~~~~~~~

Først find kunden i OptikPartner, og gå til ikonet **Overfør** lige under kundens personlige informationer, herefter kan du integrer med andre systemer.

.. image:: /images/zeiss-find-customer.png
   :width: 800


Overfør data
~~~~~~~~~~~~

Klik nu på knappen til venstre **Overfør til VisuConsult**, og dataene bliver nu overført til din ZEISS VisuConsult igennem LiloApp.

.. image:: /images/zeiss-transfer-data.png
   :width: 800


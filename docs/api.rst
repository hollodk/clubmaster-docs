API
===

API forbindelse til OptikPartner.

Først skal du sikre dig at du har en token i OptikPartner der tillader at du benytter dig af API adgang, gå til **Indstillinger > API**, og lav en ny token her.


Intro
-----

Følgende gør sig gældende for alle requests til vores API.

  | hostname:
  | https://app.optikpartner.dk

Headers:

  | For all requests
  | X-AUTH-ORGANIZATION: [PUBLIC_SECRET]

  | For authenticated endpoints:
  | X-AUTH-TOKEN: [USER_SECRET]


Bruger login
------------

Først, lav en authentication for kunden.

Endpoint: **/api/v1/auth**

Query parameters:

  | username=[USERNAME]
  | password=[PASSWORD]

Return:

  | {
  |     "status": "ok",
  |     "token": "1234-1234-1234-1234"
  | }

Eksempel:

  | curl -H "X-AUTH-ORGANIZATION: [SECRET]" https://app.optikpartner.dk/api/v1/auth?username=[USERNAME]&password=[PASSWORD]


Profile information
-------------------

Husk at brug authenticated headers til dette kald.

Endpoint: **/api/v1/me**

Return:

  | {
  |     "status": "ok",
  |     "user": {
  |         "id": 102,
  |         "username": "tidy",
  |         "firstname": "Michael 1",
  |         "lastname": "Kristensen 1",
  |         "fullname": "Michael 1 Kristensen 1",
  |         "created_at": "2020-01-08 17:12:22",
  |         "updated_at": "2021-12-27 16:02:21"
  |     },
  |     "journals": [],
  |     "sales": [
  |         {
  |             "id": 46408,
  |             "receipt": "295",
  |             "amount": "100.00",
  |             "paid_at": "2021-12-05 16:44:53",
  |             "created_at": "2021-12-05 16:44:36",
  |             "updated_at": "2021-12-05 16:53:52"
  |         }
  |     ]
  |  }


Eksempel:

  | curl -H "X-AUTH-ORGANIZATION: [SECRET]"-H "X-AUTH-TOKEN: [SECRET]" https://app.optikpartner.dk/api/v1/me


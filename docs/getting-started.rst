Kom godt i gang med OptikPartner
================================

Vi vil selvfølgelig gerne hjælpe dig godt i gang med OptikPartner, og derfor har vi lavet en lille opstartsguide, der giver en hurtig orientering i nogle af de mest benyttede dele af systemet og hjælper til at skabe gode rutiner, der sparer tid i hverdagen.

I det følgende vil vi derfor kort gennemgå:



.. toctree::
    :maxdepth: 1

    /docs/getting-started/user-new
    /docs/getting-started/eyetest
    /docs/getting-started/glassorder
    /docs/getting-started/task-sale
    /docs/getting-started/contactlensorder
    /docs/getting-started/contactlensorder-subscription
    /docs/getting-started/pos
    /docs/getting-started/pos-refund
    /docs/getting-started/user-payment
    /docs/subscription/payment-agreement



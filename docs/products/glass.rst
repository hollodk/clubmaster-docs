Glas database
=============

Læs med her, for at forstå hvordan du nemt kan redigere i glasdatabasen med egne priser.


Redigering af priser
--------------------

Glas produkter er specielle, disse produkter bliver automatisk importeret i OptikPartner fra vores leverandører.

Så snart du har solgt et glas 1 gang, så bliver det automatisk importeret i dit produkt katalog i.

Når du vil redigere dine glas, så er den letteste måde via glasbestillingen.

Start en glasbestilling og find dit ønsket glas, klik derefter på knappen "...", og en menu åbner sig hvor du kan se ekstra information om glasset, samt en knap til at redigere priserne.

.. image:: /images/products-glass-edit.png
   :width: 800


Disse produkter er lette at genkende i dit produkt katalog, der er et stykke værktøj ud for produkterne som OptikPartner automatisk har importeret.

.. image:: /images/products-glass-search.png
   :width: 800


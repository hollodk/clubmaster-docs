Regnskabsadministration
=======================

Ved synkroniseringsfejl
-----------------------

Der kan opstå synkroniseringsfejl til diverse regnskabssystemer, lagt de fleste gange er det når et nyt produkt er oprettet.

Det vil typisk være:

- Glemt at oprette moms kode
- Glemt at oprette produkt gruppe
- Produkt gruppe mangler i økonomisystem
- SKU koden er ugyldig

Fælles for dem alle er, at disse synkroniseringsfejl samler vi under **Indstillinger > System > Log > Accounting**, her kan man se alt vi sender til regnskabssystemerne, og hvad der er gået galt.


Fejlsøgning
-----------

Find fejloverførsler
~~~~~~~~~~~~~~~~~~~~

Gå til **Indstillinger > System > Log > Accounting**, herfra kan du sortere på alle fejl som vist herunder.

I den lille tekst under hver fejl vil der oftest stå problemet for synkroniseringen, og dermed også hvad der skal til for at løse problemet.

For at gå til posteringslinierne, vælger du blot **posteringer** ud for den linie du ønsker at arbejde på.

.. image:: /images/accounting-fix-ledger.png
   :width: 800


Løs problemet
~~~~~~~~~~~~~

Løs nu det problem som måtte være, hiv endelig fat i vores support hvis du har brug for hjælp til hvordan problemet afhjælpes.

.. image:: /images/accounting-fix-overview.png
   :width: 800


Begynd en resynkronisering
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Når du har løst hvad du kan, så kan du klikke på knappen **Resync**, herefter får du mulighed for at enten synkronisere:

- Hele salget inkl. betalinger
- Blot salget
- Blot betaling
- Kun denne ene linie

Herefter vil dataene indenfor få minutter være i dit regnskabssystem.

.. image:: /images/accounting-fix-resync.png
   :width: 800

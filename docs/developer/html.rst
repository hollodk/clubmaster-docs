HTML getting started
====================

Kom igang med at lær noget basal HTML :)


Hurtig introduktion
-------------------

HTML kan reelt set splittes op i 3 små elementer.

- HTML header
- HTML content
- Stylesheet


Fuld eksempel
^^^^^^^^^^^^^

Her er først et fuld eksempel på hvordan man skriver HTML.

.. code-block::

    <html>
        <head>
            <style>
            body {
                background-color: RED;
            }
            </style>
        </head>
        <body>
            <h1>Velkommen kære kunde</h1>
            <p>Vi glæder os til et lagt og godt samarbejde med dig :)</p>
            <p>Disse paragraffer kan du lave lige så mange af du vil
            og faktisk er det ligemeget med mellemrum her, hvis du vil lave et mellemrum skal du bruger<br>
            så kommer du ned på en ny linie :)</p>
        </body>
    </html>


HTML header
^^^^^^^^^^^

Først det lidt kedelige, en HTML header er blot noget lidt teknisk som fortæller browseren (ex. Chrome), hvordan siden skal præsenteres.

Det er altså denne del.

.. code-block::

    <html>
        <head>
            <style>
            body {
                background-color: RED;
            }
            </style>
        </head>


Den første linie <html> skal altid være der, det er blot en standard som hjælper browseren med at forstå nu kommer der HTML.

Den næste del <head>, fortæller at nu er der noget **meta** data, altså noget som beskriver hvordan dette dokument skal se ud.

Og så kommer den spændende del <style>, den gør det muligt at sætte farver og skrifttyper på alting, der kan gøres rigtig meget her.


HTML content
^^^^^^^^^^^^

Det næste er content, her er alt indholdet altså bare ren og skær tekst, så er det ene og alene op til stylesheet at fortælle hvordan ting skal se ud.

.. code-block::

    <body>
        <h1>Velkommen kære kunde</h1>

        <p>Vi glæder os til et lagt og godt samarbejde med dig :)</p>
        <p>Disse paragraffer kan du lave lige så mange af du vil
        og faktisk er det ligemeget med mellemrum her, hvis du vil lave et mellemrum skal du bruger<br>
        så kommer du ned på en ny linie :)</p>
    </body>


Delen <body>, fortæller browseren at nu kommer der noget content den skal indsætte på siden.

Inde i indholdet kan du se <h1> dette er en overskrift, og en <p> er en paragraf, prøv at sætte disse ting ind i en fil, og åben den fil med browseren, så kan du se hvordan ting ser ud :)


Stylesheet
^^^^^^^^^^

Stylesheet er ganske spændende, den kan rette alle elementer på en side.

.. code-block::

    <style>
        body {
            background-color: RED;
        }
    </style>


Det starter blot med en <style> som skal lægge i headeren af dokumentet, herefter kan man bruge alle mulige elementer, men i vores simple eksempel, så laver vi baggrunden **rød**.

Man kunne også lege med ting som.

.. code-block::

    <style>
        body {
            margin: 0;
            background-color: RED;
            font-size: 12px;
            font-family: serif;
            color: BLACK;
        }
        h1 {
            font-size: 20px;
            color: BLUE;
        }
    </style>


Ovenstående eksempel vil lave en side som:
- først fjerner vi alt margin, så vi ikke har noget plads til kanten af vinduet
- så gør vi baggrunden **rød**
- så sætter vi standard skriftstørrelse til **12px**
- så sætter vi skrifttypen til **serif**
- og til slut skriftfarven til **sort**

dernæst kommer så en sektion som hedder <h1>, den gør sig kun gældende for alle <h1> elementer, altså alle overskrifter.

I dette eksempel vælger vi at gøre skriftstørrelsen til **20px** og skifter farven ud med **blå** i stedet for standard sort.


Videre inspiration
------------------

Du kommer meget nemt igennem HTML ved at følge inspiration på hjemmesiden https://html.com/.

En lær CSS på 5 minutter guide her, https://www.freecodecamp.org/news/get-started-with-css-in-5-minutes-e0804813fc3e/

En anden rigtig god guide til css, https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/Getting_started


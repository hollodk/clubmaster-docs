Afbetalingsordning
==================

I OptikPartner kan du oprette afdragsordninger, som automatisk hæver pengene fra dine punkter i et aftalt interval.

Du kan naturligvis både bruge vores ResursBanks integration, men hvis du selv vil stå for afdragene og trække penge fra dine kunder via kreditkort, og spare dyre gebyrer kan du læse videre her.


Opret afdragsordning
^^^^^^^^^^^^^^^^^^^^

Først gå til kunden du ønsker at oprette en afdragsordning på.

.. image:: /images/installment-user.png
   :width: 800


Gå til salgsfanen, og gå ned til boksen som hedder **Afdragsordning**, her kan du vælge knappen **Opret ny**.

.. image:: /images/installment-create.png
   :width: 800


Overblik
^^^^^^^^

Du kommer nu til del oprettet betalingsaftale, hvor du kan se fremtidige opkrævninger.

Betalingerne vil automatisk blive trukket fra kundens kreditkort på de anførte datoer.

.. image:: /images/installment-overview.png
   :width: 800


Opret betalingsaftale
^^^^^^^^^^^^^^^^^^^^^

Hvis kunden endnu ikke har oprettet en betalingsordning, kan du nu se en boks i toppen med en rød streg, hvor der står at betalingsaftale mangler.

Følg vejledningen her for at oprette en betalingsaftale.

:doc:`/docs/subscription/payment-agreement`.


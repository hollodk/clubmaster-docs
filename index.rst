.. OptikPartner documentation master file, created by
   sphinx-quickstart on Sat Oct 23 07:00:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OptikPartner's documentation!
========================================

Topics
------

.. toctree::
    :maxdepth: 2

    docs/getting-started
    docs/subscription
    docs/user
    docs/hardware
    docs/pos/index
    docs/hotkeys
    docs/integration
    docs/kalender
    docs/warehouse
    docs/reminder
    docs/products
    docs/sale
    docs/installment
    docs/report
    docs/timetracking
    docs/synsproeve
    docs/api
    docs/resurs-bank
    docs/optics
    docs/account
    docs/configuration
    docs/marketing
    docs/plugin
    docs/administration/accounting/index
    docs/partners/index


Skriv dokumentation
-------------------

For at skrive og hjælpe til med dokumentationen, gå til :doc:`docs/developer`.

Find flere hjælpeværktøjer på :doc:`docs/scripts`
